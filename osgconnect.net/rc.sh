alias itb_status="condor_status -pool $itbcm"
alias pilot_status="condor_status -const '(slotid==1 && slottype!=\"dynamic\")'"
alias itb_pilot_status="condor_status -pool $itbcm -const '(slotid==1 && slottype!=\"dynamic\")'"
# vim:ft=sh
