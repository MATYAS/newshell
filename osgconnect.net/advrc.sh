# stores the name of my EP in $myep; leaves it alone if the EP is not found
getmyep () {
    local prog ret _myep
    prog="$HOME/gridtest/machine-queries/get-my-ep"
    _myep="$($prog $*)"
    ret=$?
    if [[ -n $_myep ]]; then
        myep=$_myep
        echo $_myep
    else
        return $ret
    fi
}
