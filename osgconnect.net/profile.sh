if $is_login
then
    export factory=gfactory-2.opensciencegrid.org
    export itbfactory=gfactory-itb-1.opensciencegrid.org
    export cm1=cm-1.ospool.osg-htc.org
    export cm2=cm-2.ospool.osg-htc.org
    export itbcm=cm-1.ospool-itb.osg-htc.org
    export ehtcm=htcondor-cm-eht.osg.chtc.io
fi

# vim:ft=sh
