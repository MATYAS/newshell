DOTDIR=${DOTDIR:-$HOME}

pathprepend "$DOTDIR/repos/newutils/fun" PATH
pathprepend "$DOTDIR/repos/newutils/linux" PATH
pathprepend "$DOTDIR/repos/newutils/public" PATH
pathprepend "$DOTDIR/repos/newutils/public/tiny" PATH
pathprepend "$DOTDIR/repos/newutils/unix" PATH

export EMAIL=matyasbot@gmail.com
export k3simages=/var/lib/rancher/k3s/agent/images

if $is_login; then
    test -d /dev/shm/me || ( umask 077; mkdir -p /dev/shm/me )
    test -d "$DOTDIR/shm" && export TMPPATH=$DOTDIR/shm
fi
