ort () {
    local e p P b g q D opt ret
    e=1
    OPTIND=1  # "The shell does not reset OPTIND automatically; it must be manually reset between multiple calls to getopts"
    while getopts ":eEPp:b:hg:G:qD" opt "$@"; do
        case $opt in
            e)  e=1
                ;;
            E)  e=
                ;;
            g)  g="-g $OPTARG"
                ;;
            G)  g="-g matyasselmeci:$OPTARG"
                ;;
            p)  params=${OPTARG%/parameters.d}
                params=$(basename "$params")
                params=${params#run-}
                p="-p $params"
                ;;
            P)  P="-P"
                ;;
            b)  b="$OPTARG"
                ;;
            q)  q=1
                ;;
            D)  D=1
                ;;
            h)  echo "Usage: ort [<options>] description..." >&2
                echo "Options:" >&2
                echo "  -g ghuser:branch       Take vm-test-runs from the given user's fork and branch" >&2
                echo "  -G branch              Take vm-test-runs from matyasselmeci's fork and the given branch" >&2
                echo "  -p <YYYYMMDD-hhmm>     Use params from a previous test run" >&2
                echo "  -P                     Use prerelease params" >&2
                echo "  -b <branch>            Use the named branch from matyasselmeci repo" >&2
                echo "  -E                     Do not edit parameters" >&2
                echo "  -q                     Quiet -- only send email to me" >&2
                echo "  -D                     Read description from stdin" >&2
                return 0
                ;;
            \?) echo "Invalid option -$OPTARG" >&2
                return 2
                ;;
        esac
    done
    [[ $OPTIND -gt 0 ]] && shift $(( OPTIND - 1 ))
    if [[ -z $D ]]; then
        [[ $# -gt 0 ]] || { echo 'need a description'; return 2; }
        description="$*"
    else
        echo -n "Description> "
        read -r description
    fi
    if [[ -n $b ]]; then
        if ! wget "https://github.com/matyasselmeci/osg-test/tree/$b" -O- &> /dev/null; then
            echo "Branch $b not found" >&2
            return 1
        fi
    fi
    output=`osg-run-tests $g $P $p "$description"`; ret=$?
    if [[ $ret == 0 ]]; then
        cd "$(grep 'Run directory' <<< "$output"  |  awk '{print $3}')"
        if [[ -n $b ]]; then
            sed -r -i -e "s|- opensciencegrid:[^;]*;|- matyasselmeci:$b;|" parameters.d/*.yaml
        fi
        if [[ -n $q ]]; then
            patch -p1 <<'TLDR'
diff --git i/bin/email-analysis w/bin/email-analysis
index 31a7e6f..94091f5 100755
--- i/bin/email-analysis
+++ w/bin/email-analysis
@@ -10,12 +10,7 @@ import vmu
 # E-mail recipient list constructed from select Software and Release Team members:
 # https://opensciencegrid.github.io/technology/
 RECIPIENTS = (
-    'Brian Lin <blin@cs.wisc.edu>',
-    'Diego Davila <didavila@ucsd.edu>',
     'Mat Selmeci <matyas@cs.wisc.edu>',
-    'Matt Westphall <westphall@wisc.edu>',
-    'Tim Cartwright <cat@cs.wisc.edu>',
-    'Tim Theisen <tim@cs.wisc.edu>'
     )
 
 ANALYSIS_FILENAME = 'analyze-test-run.out'
TLDR
        fi
        if [[ -n $e ]]; then
            vim parameters.d/*.yaml
        fi
    else
        echo "osg-run-tests failed"
        echo "$output"
        return $ret
    fi
}


submittest () {
    (
        if [[ ! -e master-run.sh ]]; then
            if [[ -e ../master-run.sh ]]; then
                cd ..
            else
                echo >&2 "can't find master-run.sh here or in parent"
            fi
        fi

        bash master-run.sh
        echo >&2 "gonna check in a couple of seconds"
        sleep 30
        condor_q -dag -nob matyas | head -n $(( ${LINES:-25} - 2 ))
    )
}


dupetest () {
    ort -p "$PWD" "$@"
}

