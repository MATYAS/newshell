#!/bin/bash


FILES=$(cat - <<EOF
.bash_profile bash_profile.sh
.bashrc       bashrc.sh
.inputrc      inputrc
.profile      profile.sh
.zprofile     zprofile.zsh
.zshenv       zshenv.zsh
.zshrc        zshrc.zsh
.zsh          zsh
.pythonrc.py  pythonrc.py
.config/fish  fish
EOF
     )


usage () {
    echo "Usage: $(basename "$0") [options] <target dir>"
    echo
    echo "Install dotfiles into target dir."
    echo
    echo "Options:"
    echo
    echo "-c    Copy files (instead of making symlinks)"
    echo "-h    Display this message"
    echo "-n    Dry-run: show what would be done"
}


wehave () {
	type -f "$@" &> /dev/null
}


cmd="ln -snf"
cmd2=$cmd
dry_run=false

while getopts :hcn OPT; do
    case $OPT in
        (c)
            cmd="cp -rf"
            ;;
        (h)
            usage
            exit 0
            ;;
        (n)
            dry_run=true
            ;;
        (*)
            usage
            exit 2
    esac
done
shift $(( OPTIND - 1 ))
OPTIND=1
if (( $# != 1 )); then
    usage
    exit 2
fi

$dry_run && cmd="echo $cmd"
$dry_run && cmd2="echo $cmd2"

target_dir=$1
source_dir=$(dirname "$0")

realpath () {
    if command -v python >/dev/null 2>&1; then
        python -c 'import os, sys; print(os.path.realpath(sys.argv[1]))' "$1"
    elif command -v python2 >/dev/null 2>&1; then
        python2 -c 'import os, sys; print(os.path.realpath(sys.argv[1]))' "$1"
    elif command -v python3 >/dev/null 2>&1; then
        python3 -c 'import os, sys; print(os.path.realpath(sys.argv[1]))' "$1"
    elif command -v perl >/dev/null 2>&1; then
        perl -MCwd -le 'print Cwd::realpath($ARGV[0])' "$1"
    else
        readlink -f "$1"
    fi
}

$cmd \
     "$(realpath "$source_dir")" \
     "$target_dir/.shell"

if ! $dry_run; then
    mkdir -p "$target_dir/.config"
fi

while read -r dest source; do
    [[ $source ]] || continue
    $cmd2 \
         "$target_dir/.shell/$source" \
         "$target_dir/$dest"
done <<<"$FILES"

