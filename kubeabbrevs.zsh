# Source me

abbrev-alias k8=kubectl
abbrev-alias k8cre="kubectl create -f"
abbrev-alias k8cree="kubectl create --edit -f"
abbrev-alias k8appf="kubectl apply -f"
abbrev-alias k8appk="kubectl apply -k"
abbrev-alias k8edi="kubectl edit"
abbrev-alias k8get="kubectl get"
abbrev-alias k8getp="kubectl get pod"
abbrev-alias k8getd="kubectl get deployment"
abbrev-alias k8getn="kubectl get node"
abbrev-alias k8des="kubectl describe"
abbrev-alias k8desp="kubectl describe pod"
abbrev-alias k8desd="kubectl describe deployment"
abbrev-alias k8del="kubectl delete"
abbrev-alias k8delp="kubectl delete pod"
abbrev-alias k8deld="kubectl delete deployment"
abbrev-alias k8delk="kubectl delete -k"
abbrev-alias k8execit="kubectl exec -i -t"
abbrev-alias k8context="kubectl config use-context"
abbrev-alias k8setns="kubectl config set-context --current --namespace"

# vim: ft=zsh:noet:sw=8:ts=8:sts=8
