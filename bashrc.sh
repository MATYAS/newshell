# shellcheck disable=SC1091
# Usage: ssource filename
ssource () {
    if [[ -r $1 ]]; then
        local _oldopts=$-
        set +u # unset nounset
        set +C # unset noclobber
        . "$1"
        expr $_oldopts : '.*u' > /dev/null && set -u
        expr $_oldopts : '.*C' > /dev/null && set -C
    fi
}

DOTDIR=${DOTDIR:-$HOME}

ssource $DOTDIR/.profile
ssource $DOTDIR/.shell/rc.sh


wehave sudo && alias please='sudo $(history -p !!)'

if wehave zsh; then
    alias runzsh="history -w; exec /usr/bin/env SHELL=`which zsh` zsh -il"
fi

# don't put duplicate lines in the history. See bash(1) for more options
# ... or force ignoredups and ignorespace
HISTCONTROL=ignoredups:ignorespace
HISTSIZE=99999
HISTFILESIZE=99999
if [[ $SINGULARITY_NAME ]]; then
    _singularity_name_safe=$(echo -n "$SINGULARITY_NAME" | tr -s -c '[:alnum:]' _)
    HISTFILE=$HOME/.bash_history_${_singularity_name_safe}
else
    HISTFILE=$HOME/.bash_history
fi
#HISTTIMEFORMAT="%Y-%m-%d %H:%M:%S"
unset HISTTIMEFORMAT

# append to the history file, don't overwrite it
shopt -s histappend

# put embedded newlines in history
shopt -s lithist

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

set -o nounset
set -o noclobber

set -o physical
# ^ resolve symlinks when cd'ing

shopt -s extglob
shopt -s gnu_errfmt
if [[ ${BASH_VERSION%%.*} -gt 4 ]]; then
    shopt -s globstar  # accept "**"
fi

unset PROMPT_COMMAND
notblind() {
    local invert reset
    invert="\033[1;4;7m"
    reset="\033[0m"

    PS1=""
    case $TERM in
        xterm*|rxvt*|screen*)
            PS1="\[\033]0;\u@\h:\w\007\]"
            ;;
    esac
    PS1="${PS1PRE-}${PS1}\[${invert}\][\$?"
    [[ `id -u` == 0 ]] && PS1="${PS1} [ROOT]"
    PS1="${PS1} \A \u@"
    if [[ ! ${SINGULARITY_NAME-} ]]; then
        PS1="${PS1}\h"
    else
        PS1="${PS1}${SINGULARITY_NAME}"
    fi
    PS1="${PS1} \w]\[${reset}\]\n"
    PS1="${PS1}\[${invert}\][\!]\$\[${reset}\] "
    PS2="\[${invert}\]===============>\[${reset}\] "
}
blind() {
    PS1="\[[1m[4m[7m\]"
    [[ `id -u` == 0 ]] && PS1="${PS1}ROOT "
    PS1="${PS1}bash enter command...        \[[0m\]"
    PS2="\[[1m[4m[7m\]command continues...        \[[0m\]"
}
notblind
export PS1 PS2

if [[ ${TERM-} == dumb ]]; then
    PS1='$ '
    PS2='> '
    unset PROMPT_COMMAND
fi

set -o vi
bind -m vi-insert "\C-l":clear-screen
bind -m vi-command "\C-z":emacs-editing-mode
bind -m emacs "\C-z":vi-editing-mode

bind -m vi-insert Space:magic-space

[[ -r /etc/bash_completion || -d /etc/bash_completion.d || -d /usr/share/bash-completion || -f /usr/local/etc/bash_completion ]] && set +o nounset
if [[ -r /etc/bash_completion ]]; then
    . /etc/bash_completion
elif [[ -d /etc/bash_completion.d ]]; then
    for f in /etc/bash_completion.d/*; do
        ssource "/etc/bash_completion.d/$f"
    done
elif [[ -f /usr/local/etc/bash_completion ]]; then
    . /usr/local/etc/bash_completion
fi


# Turn off backslashing variable names when doing path completion
_tmp=$(type -a _filedir 2>/dev/null)  \
    && {
    _tmpf=$(mktemp)
    tail -n +2 >| "$_tmpf" <<<"$_tmp"
    sed -i.bak -e 's|compopt -o filenames 2>|compopt -r filenames -o noquote 2>|' "$_tmpf"
    . "$_tmpf"
    rm -f "$_tmpf" "${_tmpf}.bak"
    unset _tmpf
}



# My own bash completions
_fromtemplate () {
    local cur prev
    COMPREPLY=()
    cur=${COMP_WORDS[COMP_CWORD]}
    prev=${COMP_WORDS[COMP_CWORD-1]}

    if [[ ${#COMP_WORDS[*]} -eq 2 ]]; then
        templates=($(compgen -G "${TEMPLATE_DIR:-$HOME/.templates}/${cur}*") )
        for t in "${templates[@]}"; do
            COMPREPLY+=("${t#${TEMPLATE_DIR:-$HOME/.templates}/}")
        done
    elif [[ ${#COMP_WORDS[*]} -gt 2 ]]; then
        COMPREPLY=($(compgen -f))
    fi

    return 0
}
complete -F _fromtemplate fromtemplate

wehave kustomize && complete -C "$(which kustomize)" kustomize



# vim:ft=sh
