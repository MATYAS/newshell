set ann /Users/Shared/Annex
set mus /Users/Shared/Music

set -x LANG en_US.UTF-8
if test ms = "$USER" || test me = "$USER"
    set -a PATH "/Users/$USER/Library/Android/sdk/platform-tools"
    set -x EMAIL matyasbot@gmail.com
    test -r "/Users/$USER/condor/condor.fish" &&
        . "/Users/$USER/condor/condor.fish"
    set -x ANSIBLE_INVENTORY "/Users/$USER/repos/ansible/inventory-alice"
else if test mat = $USER; then
    set -x EMAIL matyas@cs.wisc.edu
end

if test matyas = (id -gn) && test 0 -ne (id -u)
    umask 002
end

set -p PATH "/usr/local/bin" "/opt/local/bin" "/opt/local/sbin"
set -p PATH "$DOTDIR/repos/newutils/fun" \
            "$DOTDIR/repos/newutils/mac" \
            "$DOTDIR/repos/newutils/public" \
            "$DOTDIR/repos/newutils/public/tiny" \
            "$DOTDIR/repos/newutils/unix" 

