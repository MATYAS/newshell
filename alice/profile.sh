export ann=/Users/Shared/Annex
export mus=/Users/Shared/Music
DOTDIR=${DOTDIR:-$HOME}

export LANG=en_US.UTF-8
if test ms = $USER || test me = $USER; then
    pathappend "/Users/$USER/Library/Android/sdk/platform-tools"
    export EMAIL=matyasbot@gmail.com
    ssource "/Users/$USER/condor/condor.sh"
    export ANSIBLE_INVENTORY="/Users/$USER/repos/ansible/inventory-alice"
elif test mat = $USER; then
    export EMAIL=matyas@cs.wisc.edu
fi

if test matyas = `id -gn` && test 0 -ne `id -u`; then
    umask 002
fi

pathprepend "/opt/local/sbin" PATH
pathprepend "/opt/local/bin" PATH
#pathprepend "/usr/local/lib/python2.7/site-packages" PYTHONPATH
pathprepend "/usr/local/bin" PATH

pathprepend "$DOTDIR/repos/newutils/fun" PATH
pathprepend "$DOTDIR/repos/newutils/mac" PATH
pathprepend "$DOTDIR/repos/newutils/public" PATH
pathprepend "$DOTDIR/repos/newutils/public/tiny" PATH
pathprepend "$DOTDIR/repos/newutils/unix" PATH
