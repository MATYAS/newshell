export m=$HOME
export ob=$HOME/PycharmProjects/osg-build
DOTDIR=${DOTDIR:-$HOME}

pathprepend "/opt/idea/bin" PATH
pathprepend "$DOTDIR/repos/newutils/csl" PATH
pathprepend "$DOTDIR/repos/newutils/fun" PATH
pathprepend "$DOTDIR/repos/newutils/linux" PATH
pathprepend "$DOTDIR/repos/newutils/public" PATH
pathprepend "$DOTDIR/repos/newutils/public/tiny" PATH
pathprepend "$DOTDIR/repos/newutils/unix" PATH
pathappend "$DOTDIR/.cargo/bin" PATH

export LANG=en_US.utf8
export LC_CTYPE=en_US.utf8
export LC_NUMERIC=C
export LC_TIME=C
export LC_COLLATE=C
export LC_MONETARY=C
export LC_MESSAGES=C
export LC_PAPER=C
export LC_NAME=C
export LC_ADDRESS=C
export LC_TELEPHONE=C
export LC_MEASUREMENT=C
export LC_IDENTIFICATION=C

export DOCKER_HOST=unix:///run/user/$UID/podman/podman.sock

export BROWSER=bestbrowser
