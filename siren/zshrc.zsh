case $TERM in
    xterm*|rxvt*|screen*)
        precmd () {
            _precmd_vcs
            print -Pn "\e]0;%3~\a"
            setprompt
        }
        ;;
    dumb)
        precmd () { : }
        ;;
    *)
        precmd () {
            _precmd_vcs
            setprompt
        }
        ;;
esac

# vim:ft=zsh
