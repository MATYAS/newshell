# neomutt signing is broken on ingwe for some reason
wehave neomutt && alias mutt=neomutt

wehave vimx && alias vim=vimx

aerc () {
    LESS=${LESS/--chop-long-lines/} command aerc
}

alias sman="man --html=surf"

ssource "$HOME/.travis/travis.sh"

#darken () {
#    local mult=${1:-1}
#    sudo backlight --set-brightness $(( `backlight --get-brightness` - 10 * mult ))
#}
#
#brighten () {
#    local mult=${1:-1}
#    sudo backlight --set-brightness $(( `backlight --get-brightness` + 10 * mult ))
#}
