### To profile startup, uncomment 'zmodload zsh/zprof' here and also 'zprof' at the end of this file
# zmodload zsh/zprof

# Usage: ssource filename
ssource () {
    if [[ -r $1 ]]; then
        local _oldopts=$-
        set +u # unset nounset
        set +C # unset noclobber
        . "$1"
        expr $_oldopts : '.*u' > /dev/null && set -u
        expr $_oldopts : '.*C' > /dev/null && set -C
    fi
}

DOTDIR=${DOTDIR:-$HOME}
ZDOTDIR=${ZDOTDIR:-$DOTDIR}

TERM=${TERM:-dumb}
COLORS=$(tput colors 2> /dev/null || echotc Co 2>/dev/null)


set +u
set +C
for file (  "$ZDOTDIR/.zprofile"
            "$ZDOTDIR/.shell/rc.sh"
            "$ZDOTDIR/.zsh/aliases"
            "$ZDOTDIR/.zsh/functions"
            "$ZDOTDIR/.zsh/interactive_local"
        ); do
    [[ -r $file ]] && . "$file"
done


# hostname -f is slow when the network is shitty
full_hostname=$(uname -n)
short_hostname=${full_hostname%%.*}
domain_name=${full_hostname#*.}


HISTSIZE=99999
SAVEHIST=99999
HISTFILE=$HOME/.zsh_history

fpath=($ZDOTDIR/.zsh/functions $ZDOTDIR/.zsh/prompts $fpath)

if [[ $domain_name != "osgconnect.net" ]]; then
    # vcs_info is slow on osg-connect's shared FS
    autoload -Uz vcs_info
fi
if vcs_info &> /dev/null; then  # function actually got loaded
    zstyle ':vcs_info:*' enable git svn
    zstyle ':vcs_info:*' formats "%b"
    zstyle ':vcs_info:*' actionformats "%b (%a)"
    _precmd_vcs () {
        vcs_info 2>/dev/null
        # The VCS branch if there is one
        pmt_vcsinfo="${vcs_info_msg_0_:+[${emoji_git-}${vcs_info_msg_0_}] }"
    }
else
    _precmd_vcs () {
        pmt_vcsinfo=""
    }
fi

_precmd_kube () {
    # override this in per-host config
    pmt_kubeinfo=""
}

# %S%U%B = standout, underline, and bold; %b%u%s = turn off bold, underline, standout
# %F, %K = set foreground, background; %k, %f = reset foreground, background
pmt_style='%S%U%B'
pmt_endstyle='%b%u%s'
pmt_user=%n@
if [[ $USER == m[es] || $USER == [Mm]atyas* ]]; then
    pmt_user=
fi

pmt_machine='%m'
pmt_machinet='[%m] '
case $short_hostname in
    BET-C) pmt_machine='[B]'; pmt_machinet='[B] ' ;;
    siren) pmt_machine='[s]'; pmt_machinet='[s] ' ;;
    nereid2) 
        case ${WSL_DISTRO_NAME:-Ubuntu} in
            Ubuntu) pmt_machine='[n-u20]'; pmt_machinet='[n-u20] ' ;;
            Ubuntu-22.04)
                pmt_machine='[n-u22]'; pmt_machinet='[n-u22] '
                if [[ $COLORS == 256 ]]; then
                    pmt_style='%F{#000}%K{#ca8}%U%B'
                    pmt_endstyle='%b%u%k%f'
                fi
                ;;
            OracleLinux_8*) pmt_machine='[n-o8]'; pmt_machinet='[n-o8] ' ;;
            AlmaLinux-8*) pmt_machine='[n-a8]'; pmt_machinet='[n-a8] ' ;;
            AlmaLinux9*)
                pmt_machine='[n-a9]'; pmt_machinet='[n-a9] '
                if [[ $COLORS == 256 ]]; then
                    pmt_style='%F{#333}%K{#99f}%U%B'
                    pmt_endstyle='%b%u%k%f'
                fi
                ;;
            *) pmt_machine='[n]'; pmt_machinet='[n] ' ;;
        esac ;;
    maya) pmt_machine='[m]'; pmt_machinet='[m] ' ;;
esac

case $short_hostname in
    BET-C|siren|nereid2|maya)
        emoji_py='🐍 '
        emoji_git='🧞 '
        ;;
    *)
        emoji_py=''
        emoji_git=''
        ;;
esac

case $TERM in
    xterm*|rxvt*|screen*)
        precmd () {
            print
            _precmd_kube
            _precmd_vcs
            #print -Pn "\e]0;${pmt_user}${pmt_machinec}%~\a"
            print -Pn "\e]0;${pmt_user}${pmt_machinet}%3~\a"
            setprompt
        }
        preexec () {
            print
        }
        ;;
    dumb)
        precmd () { : }
        ;;
    *)
        precmd () {
            print
            _precmd_kube
            _precmd_vcs
            setprompt
        }
        preexec () {
            print
        }
        ;;
esac

pmt_newline=$'\n'
# The string '[ROOT] ' if we're root, nothing otherwise
pmt_root="%(!.[ROOT] .)"
# The vcsh repo name, if exists
pmt_vcsh="${VCSH_REPO_NAME:+[vcsh: $VCSH_REPO_NAME] }"
# The retcode if nonzero
pmt_retcode="%(?..%? )"
# "J:" followed by the number of background jobs (if there are any)
pmt_jobs="%(1j.J:%j .)"
## The time
pmt_time="%T"
# username@short hostname
pmt_userhost="${pmt_user}${pmt_machine}"
# The last 5 elements of the path
pmt_path="%5~"
# '%' if non-root, '#' if root
pmt_prompt='%#'
# the history number
pmt_history='%!'
setprompt() {
    # Virtualenv if it exists
    local _venvdir
    if [[ -n ${VIRTUAL_ENV:-} ]]; then
        case $VIRTUAL_ENV in
            */venv) _venvdir=${VIRTUAL_ENV%/venv} ;;
            */env) _venvdir=${VIRTUAL_ENV%/env} ;;
            */.venv) _venvdir=${VIRTUAL_ENV%/.venv} ;;
            *) _venvdir=${VIRTUAL_ENV} ;;
        esac
    fi
    pmt_virtualenv="${VIRTUAL_ENV:+(${emoji_py-}$(basename "$_venvdir"))}"
    PS1="${pmt_style}${PS1PRE-}${pmt_virtualenv}[${pmt_root}"
    if [[ -n $pmt_vcsh ]]; then
        PS1=${PS1}"${pmt_vcsh}"
    else
        PS1=${PS1}"${pmt_vcsinfo}"
    fi
    PS1=${PS1}"${pmt_kubeinfo-}"
    PS1=${PS1}"${pmt_retcode}${pmt_jobs}${pmt_time} ${pmt_userhost}][${pmt_history}]${pmt_newline}[${pmt_path}]${pmt_prompt}${pmt_endstyle} "
    PS2="${pmt_style}%_ =======>${pmt_endstyle}  "
}
simpleprompt() {
    PS1="${pmt_style}%${PS1PRE-}(!.ROOT .)zsh enter command...    ${pmt_endstyle}"
    PS2="${pmt_style}%(!.     .)%_ continues...    ${pmt_endstyle}"
    unset -f precmd
}
verysimpleprompt() {
    # designed for copy-and-paste into slack or something
    PS1="${pmt_style}%# i${pmt_endstyle}"
    PS2="${pmt_style}  ${pmt_endstyle}"
    unset -f precmd
}
setprompt


if [[ ${TERM-} == screen-bce ]] ; then
    export TERM=screen
fi

if [[ -n ${SNAME-} ]]; then
    screen -X hardstatus string "${SNAME-} --- %w"
fi

### Shell options ### {{{
setopt                          \
        always_last_prompt      \
        auto_pushd              \
        bg_nice                 \
     NO_correct                 \
     NO_dvorak                  \
        extended_glob           \
        extended_history        \
     NO_hist_find_no_dups       \
     NO_hist_ignore_all_dups    \
        hist_ignore_dups        \
        hist_ignore_space       \
        hist_save_no_dups       \
        hist_subst_pattern      \
        inc_append_history      \
        interactive_comments    \
        kshglob                 \
        no_hup                  \
     NO_nomatch                 \
        prompt_subst            \
        pushd_ignore_dups       \
        pushd_silent            \
        sh_word_split           \
        nounset                 \
        noclobber

setopt chase_links  # resolve symlinks when cd'ing, same as "physical" in bash

typeset REPORTTIME=5
# ^ report timing info if command ran for more than 5 seconds

# }}}

wehave dircolors && eval `dircolors --sh` && LS_COLORS="$LS_COLORS:ow=4;34:ex=0;32:ln=0;36"

disable log
# ^ I never use this builtin (it just lists logged in users) and there's a
# script with the same name I want to use instead

# completion {{{
autoload -Uz compinit
# -u ignores 'insecure' directories
compinit -u

compctl -l '' run myrun log
#compctl -/ -M 'm:{[:lower:][:upper:]}={[:upper:][:lower:]} m:[-._]=[-._]' c cl
#          ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ Case-insensitive completion
#                                                          ^^^^^^^^^^^ _ == -

zstyle ':completion:*' verbose yes
# separate manpage sections:
zstyle ':completion:*' separate-sections true
# remove trailing slash
zstyle ':completion:*' squeeze-slashes true
# auto rehash
zstyle ':completion:*' rehash true

if [[ -e /afs ]] && ! wehave mount-cs; then
    zstyle ':completion:*' completer _complete _approximate
    zstyle ':completion:*' max-errors 0 numeric
else
    zstyle ':completion:*' max-errors 0 numeric
    zstyle ':completion:*' completer _expand _complete _approximate
fi
zstyle ':completion:*' format 'Completing %d'
zstyle ':completion:*' group-name ''
zstyle ':completion:*' insert-unambiguous false
if [[ -n ${LS_COLORS-} ]]; then
    zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}
fi
zstyle ':completion:*' list-prompt '%SAt %p: Hit TAB for more, or the character to insert%s'
zstyle ':completion:*' match-original only
zstyle ':completion:*' menu select=7
zstyle ':completion:*' original true
zstyle ':completion:*' preserve-prefix '//[^/]##/'
zstyle ':completion:*' prompt '(typo? %e)'
zstyle ':completion:*' select-prompt '%SScrolling active: current selection at %p%s'
zstyle ':completion:*' use-cache on

# case-insensitive matching and hyphen matches underscore
#zstyle ':completion:*' matcher-list 'm:{[:lower:][:upper:]}={[:upper:][:lower:]} m:[-._]=[-._]'

# menu for selecting pids
zstyle ':completion:*:*:kill:*' menu yes select
zstyle ':completion:*:kill:*' force-list always
# }}}

# these should complete directories, just like cd
compdef c=cd
compdef cl=cd
compdef cdphys=cd

compdef ssource=source

if wehave kustomize; then
    autoload -U bashcompinit && bashcompinit
    complete -o nospace -C "$(which kustomize)" kustomize
fi

if [[ $TERM == dumb ]]; then
    unsetopt zle
    unsetopt prompt_cr
    unsetopt prompt_subst
    unfunction precmd
    unfunction preexec
    PS1='$ '
    PS2='> '
else
    if wehave powerline-go && [[ -r "$SHELLDIR/powerline.zsh" ]]; then
        unfunction precmd
        unfunction preexec
        . "$SHELLDIR/powerline.zsh"
    fi
fi

autoload zmv
autoload edit-command-line
zle -N edit-command-line

bindkey -v  # vi mode
bindkey e edit-command-line
bindkey -M vicmd v edit-command-line
bindkey -M viins '^X^E' edit-command-line
bindkey -M emacs '^X^E' edit-command-line

# make some keys work like in emacs mode
bindkey -M viins '^R' history-incremental-search-backward
bindkey -M vicmd '^R' history-incremental-search-backward
bindkey -M viins '^S' history-incremental-search-forward
bindkey -M vicmd '^S' history-incremental-search-forward

bindkey -M vicmd '^O' accept-line-and-down-history
bindkey -M viins '^O' accept-line-and-down-history
bindkey -M viins '^G' send-break

# push line into stack; gets popped off after next command
bindkey -M viins 'q' push-line-or-edit
bindkey -M vicmd 'q' push-line-or-edit

# make backspace in vi mode backspace over where insertion started too
bindkey -M viins  backward-delete-char
bindkey -M viins  backward-delete-char

# pgup/pgdn history search
bindkey "[5~" history-beginning-search-backward # Page Up
bindkey "[6~" history-beginning-search-forward  # Page Down


# delete key
bindkey -M viins '[3~' delete-char
bindkey -M vicmd '[3~' vi-delete-char
bindkey -M emacs '[3~' delete-char

# home, end
bindkey -M viins '[1~' beginning-of-line
bindkey -M viins '[4~' end-of-line
bindkey -M emacs '[1~' beginning-of-line
bindkey -M emacs '[4~' end-of-line
bindkey -M emacs 'OH' beginning-of-line
bindkey -M emacs 'OF' end-of-line
bindkey -M emacs '[H' beginning-of-line
bindkey -M emacs '[F' end-of-line
bindkey -M vicmd '[1~' vi-beginning-of-line
bindkey -M vicmd '[4~' vi-end-of-line






### Aliases ###
alias mmv="noglob zmv -W"

alias ng="noglob"
alias mvbak="mkdir -p .bak && mv *~(D) .bak" # (D) makes glob match dotfiles as well

# global aliases
alias -g ,,g='| grep -E'
alias -g ,,gv='| grep -Ev'
alias -g ,,l='| less -+F'
alias -g ,,nul='&>/dev/null'
alias -g ,,s='| sort'
alias -g ,,suc='| sort | uniq -c'
alias -g ,,a='| awk'


alias -g /C='| cut'
alias -g /EG='|& egrep'
alias -g /EGF='|& grep -F'
alias -g /EL='|& less'
alias -g /G='| egrep'
alias -g /GF='| grep -F'
alias -g /GP='| grep -P'
alias -g /GV='| grep -v'
alias -g /LL="2>&1 | less -+F"
alias -g /L="| less -+F"
alias -g /NE="2> /dev/null"
alias -g /NUL="> /dev/null 2>&1"
alias -g /S='| sort'
alias -g /SUC='| sort | uniq -c'
alias -g /SS="| sed -r -e"
alias -g /X0G='| xargs -0 egrep'
alias -g /X0='| xargs -0'
alias -g /XG='| xargs egrep'
alias -g /X='| xargs'
alias -g /HL='| head -n $(( ${LINES:-25} - 1 ))'
# awk {{{
alias -g /A='| awk '
alias -g /A1="| awk '{print \$1}'"
alias -g /A2="| awk '{print \$2}'"
alias -g /A3="| awk '{print \$3}'"
alias -g /A4="| awk '{print \$4}'"
alias -g /A5="| awk '{print \$5}'"
alias -g /A6="| awk '{print \$6}'"
alias -g /A7="| awk '{print \$7}'"
alias -g /A8="| awk '{print \$8}'"
alias -g /A9="| awk '{print \$9}'"
alias -g /EA='|& awk '
alias -g /EA1="|& awk '{print \$1}'"
alias -g /EA2="|& awk '{print \$2}'"
alias -g /EA3="|& awk '{print \$3}'"
alias -g /EA4="|& awk '{print \$4}'"
alias -g /EA5="|& awk '{print \$5}'"
alias -g /EA6="|& awk '{print \$6}'"
alias -g /EA7="|& awk '{print \$7}'"
alias -g /EA8="|& awk '{print \$8}'"
alias -g /EA9="|& awk '{print \$9}'"
# }}}
alias -g /TB='`git-thisbranch`'

alias please='eval sudo "$(fc -nl -1)"'
alias editthat='$EDITOR $( $(fc -nl -1) )'



#
#
# fish-style abbreviations
#
# https://dev.to/frost/fish-style-abbreviations-in-zsh-40aa
#
#
# declare a list of expandable aliases to fill up later
typeset -a ealiases
ealiases=()

# write a function for adding an alias to the list mentioned above
function abbrev-alias() {
    alias "$1"
    ealiases+=("${1%%\=*}")
}

# expand any aliases in the current line buffer
function expand-ealias() {
    if [[ $LBUFFER =~ "\<(${(j:|:)ealiases})\$" ]]; then
        zle _expand_alias
        zle expand-word
    fi
    zle magic-space
}
zle -N expand-ealias

# Bind the space key to the expand-alias function above, so that space will expand any expandable aliases
bindkey ' '        expand-ealias
bindkey '^ '       magic-space     # control-space to bypass completion
bindkey -M isearch " "      magic-space     # normal space during searches

## Disabled: if I leave in the .backward-delete-char, it breaks history substitution,
## but if I take it out, it breaks hitting enter in the middle of a line
## A function for expanding any aliases before accepting the line as is and executing the entered command
#expand-alias-and-accept-line() {
#    expand-ealias
#    ## the .backward-delete-char breaks the '^^' history substitution
#    # zle .backward-delete-char
#    zle .accept-line
#}
#zle -N accept-line expand-alias-and-accept-line

if wehave git; then
    abbrev-alias g="git"
    abbrev-alias gsvn="git svn"
fi
abbrev-alias s="sudo"

#
# end fish-style abbreviations
#

if wehave kubectl && [[ -r "$SHELLDIR/kubeabbrevs.zsh" ]]; then
    . "$SHELLDIR/kubeabbrevs.zsh"
fi


for file ("$ZDOTDIR/.shell/$domain_name/zshrc.zsh"
          "$ZDOTDIR/.shell/$short_hostname/zshrc.zsh"
         )
do
    [[ -r $file ]] && . "$file"
done

if [[ $full_hostname != $short_hostname && -r "$ZDOTDIR/.shell/$full_hostname/zshrc.zsh" ]]; then
    . "$ZDOTDIR/.shell/$full_hostname/zshrc.zsh"
fi

:

# zprof
set -uC

# vim:ft=zsh
