# Source me

alias k8 kubectl
alias k8cre "kubectl create -f"
alias k8cree "kubectl create --edit -f"
alias k8appf "kubectl apply -f"
alias k8appk "kubectl apply -k"
alias k8edi "kubectl edit"
alias k8get "kubectl get"
alias k8getp "kubectl get pod"
alias k8getd "kubectl get deployment"
alias k8getn "kubectl get node"
alias k8des "kubectl describe"
alias k8desp "kubectl describe pod"
alias k8desd "kubectl describe deployment"
alias k8del "kubectl delete"
alias k8delp "kubectl delete pod"
alias k8deld "kubectl delete deployment"
alias k8delk "kubectl delete -k"
alias k8execit "kubectl exec -i -t"
alias k8context "kubectl config use-context"
alias k8setns "kubectl config set-context --current --namespace"

# vim: ft=fish:et:sw=4:ts=8:sts=4
