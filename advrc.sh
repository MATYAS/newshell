###############################################################################
# advrc.sh
# Advanced interactive commands for bash/zsh
###############################################################################



###############################################################################
# condor
###############################################################################
if wehave condor_q; then
    if [[ $SHELLTYPE == bash ]]; then
        function set_condor {
            local knob="${1?Need knob and value}"
            local value="${2?Need value}"
            # uppercase the knob name (bash syntax)
            knob=_CONDOR_${knob^^}
            export "${knob}"
        }

        function unset_condor {
            local knob="${1?Need knob}"
            # uppercase the knob name (bash syntax)
            knob=_CONDOR_${knob^^}
            unset "${knob}"
        }

    elif [[ $SHELLTYPE == zsh ]]; then
        function set_condor {
            local knob="${1?Need knob and value}"
            local value="${2?Need value}"
            # uppercase the knob name (zsh syntax)
            # shellcheck disable=SC2296    # shellcheck thinks this file is bash
            knob=_CONDOR_${(U)knob}
            eval "${knob}"="\"${value}\""
            export "${knob}"
        }

        function unset_condor {
            local knob="${1?Need knob}"
            # uppercase the knob name (zsh syntax)
            # shellcheck disable=SC2296    # shellcheck thinks this file is bash
            knob=_CONDOR_${(U)knob}
            unset "${knob}"
        }
    fi
fi






###############################################################################
# cd_ls
###############################################################################

# disabled for now because the ls aliases that add colors do not work on it
# any more -- probably something to do with the order they were defined in.
#
## this needs the 'function' prefix to avoid a syntax error
#function ls {
#    if [[ $PWD == */u/m/a/matyas || $PWD == /home/matyas || $PWD == $HOME ]] ; then
#        /bin/ls "$@"
#    else
#        /bin/ls -A "$@"
#    fi
#}

function uniq_first {
    # Like uniq, but doesn't require sorting the data;
    # for non-unique lines, the first line will be kept.
    perl - <<'EOF'

    my (@output, %seen);
    while (<>) {
            if (!exists($seen{$_})) {
                    push @output, $_;
                    $seen{$_} = 1;
            }
    }
    print join("", @output);

EOF
}

function reverse_cat {
    if wehave tac; then
        tac "$@"
    else
        tail -r "$@"
    fi
}

if [[ -n ${BASH-} ]]; then
    # zsh doesn't need this, there are shell variables to turn cd into pushd
    function cd {
        local ret

        # keep a directory stack, but only put unique entries into the stack.
        if [[ -z ${1-} ]]; then
            pushd "$HOME" > /dev/null
            ret=$?
        else
            pushd "$@" > /dev/null
            ret=$?
        fi

        wehave perl || return $ret

        local old_dirs
        local first

        # I have to do dirs -c in the loop because saving `dirs -p` in a
        # variable turns the newlines separating dirs into spaces for some
        # reason.
        first=1
        for d in `dirs -p -l | uniq_first | reverse_cat`; do
            if [[ $first == 1 ]]; then
                first=0
                dirs -c
            fi
            if [[ $d != $PWD ]]; then
                pushd -n "$d" > /dev/null
            fi
        done

        return $ret
    }
fi

function cl {
    local ret d thisbranch
    if (( $# > 1 )); then
        select d in "$@"; do
            cd "$d" && /bin/ls ${ls_options-}
            ret=$?
            break
        done
    else
        cd "$@" && /bin/ls ${ls_options-}
        ret=$?
    fi
    if (( ret == 0 )); then
        thisbranch=$(git symbolic-ref -q HEAD 2>/dev/null)
        [[ -n $thisbranch ]] && echo "(${thisbranch##refs/heads/})"
    fi
    return $ret
}

function cdtree {
    [[ -n ${1-} ]] && cd "$1"
    local done=0
    while (( done == 0 )); do
        select dir in '<DONE>' */; do
            if [[ '<DONE>' == $dir ]]; then
                done=1
                break
            else
                cd "$dir"
                break
            fi
        done
    done
}


function c {
    if [[ -n ${1-} ]]; then
        if [[ -d $1 ]]; then
            cl "$1"
        else
            echo "cl $1*/"
            cl "$1"*/
        fi
    else
        cl "$HOME"
    fi
}

function u {
    if [[ -n ${1-} ]]; then
        CDPATH=.:..:../..:../../.. cl "$1"
    else
        cl ..
    fi
}

function cphys {
    c "$@" && cdphys
}

function z {
    # Shortcut to cd somewhere under my home directory (which $m is normally
    # set to).
    c "${m:-$HOME}/${1-}"
}






###############################################################################
# doslike
###############################################################################

alias cls=clear
alias del=rm
alias deltree="rm -r"

function copy {
    if [[ $1 == con ]]; then
        eval "cat - ${2:+>$2}"
    else
        local flags
        while [[ $1 == -* ]]; do
            flags="$flags $1"
            shift
        done
        : ${2:=.}
        eval "cp $flags" "$@"
    fi
}
function move {
    local flags
    while [[ $1 == -* ]]; do
        flags="$flags $1"
        shift
    done
    local numargs=$#
    local lastarg
    eval lastarg=\$$#
    if [[ ! -d $lastarg ]]; then
        eval "mv $flags" "$@" .
    else
        eval "mv $flags" "$@"
    fi
}
alias ren="mv -T"
alias xcopy="copy -r"


alias totalcls="clear; tmux clear-history >/dev/null 2>&1"






###############################################################################
# edit
###############################################################################
_edit_1 () {
    local ed=$1
    shift
    if [[ $# -gt 1 ]]; then
        local f
        select f in "$@"; do
            $ed $f
            return $?
        done
    else
        $ed "$@"
    fi
}

_edit_2 () {
    local editor=$1
    shift
    if [[ -n ${1-} ]]; then
        if [[ -f $1 ]]; then
            _edit_1 $editor $1
        else
            echo "_edit_1 $editor $1*"
            _edit_1 $editor $1*
        fi
    else
        $editor
    fi
}

alias ee='_edit_2 ${EDITOR:-vim}'
alias gee='_edit_2 ${XEDITOR:-gvim}'
alias :e='ee'






###############################################################################
# mvup
###############################################################################
mvup() {
    local curdir=${PWD##*/}
    if [[ $SHELLTYPE == zsh ]]; then
        ( set -o dotglob; mv * .. )
    else
        ( shopt -s dotglob; mv * .. )
    fi
    cd ..
    rmdir "$curdir"
}






###############################################################################
# save_command / savecmd
###############################################################################
save_command () {
    local saveworkdir
    [[ $1 == -w ]] && { saveworkdir=true; shift; }
    local thealias=${1:?"Usage: savecmd [-w] ALIAS [HISTNUM]"}
    if alias "$thealias" 2> /dev/null; then
        echo $thealias is already in use.
        return 1
    fi
    local histnum
    if [[ -n ${BASH-} ]]; then
        histnum=${2:--2}
    else
        histnum=${2:--1}
    fi
    #cmd=$(fc -nl $histnum $histnum)
    # Use fc -l to print event # $histnum; use awk to compress spaces and
    # remove the number from the beginning.
    # Not using fc -nl because if I use it the behavior in zsh is different than bash:
    # in zsh, the command begins immediately; in bash, there are spaces before the command.
    cmd=$(fc -l $histnum $histnum | awk '{for (i=2; i<=NF; ++i) printf("%s%s", $(i), i==NF ? "\n" : " ")}')

    local acontents
    if [[ -n $saveworkdir ]] ; then
        acontents="pushd \"$PWD\" && { $cmd; popd; }"
    else
        acontents=$cmd
    fi
    if ask_yn "Create alias '$thealias' as '$acontents' ?"; then
        alias $thealias="$acontents" && echo "Created"
    else
        echo "Not created"
    fi
}

alias savecmd=save_command





###############################################################################
# ssh
###############################################################################
# insecure shell / copy
if wehave ssh; then
    alias sshis=sshnohostkey
    alias scpis=scpnohostkey
    alias sftpis=sftpnohostkey
    alias sshvm=sshtovm


#sshagent () {
#    export SSH_AUTH_SOCK SSH_AGENT_PID
#    if [[ -z $SSH_AUTH_SOCK ]]; then
#        local stat
#        # Hooray for platform differences in basic utils.
#        if [[ $OSTYPE == darwin* || $OSTYPE == *bsd* ]]; then
#            stat='stat -f "%Su %SN%n"'
#        else
#            stat='stat -c "%U %n"'
#        fi
#        SSH_AUTH_SOCK=$(eval $stat ${TMPDIR:-/tmp}/ssh-*/agent.* \
#                            | grep $(whoami) \
#                            | head -1 2> /dev/null \
#                            | awk '{print $2}')
#    fi
#    if [[ ! -S $SSH_AUTH_SOCK ]]; then
#        eval `ssh-agent -s` > /dev/null
#    fi
#}

    sshagent () {
        local agentfile=$HOME/.ssh/`hostname -s`-agent
        if [[ $(ps -u $USER | grep -c '[s]sh-agent') -lt 1 ]]; then
            ssh-agent -s >|"$agentfile"
        fi
        . "$agentfile"
    }


    addkeys () {
        find "${DOTDIR:-$HOME}/.ssh" \
            -name id_\* \
            -not -name \*pub\* \
            -not -name \*autossh\* \
            -exec ssh-add {} +
    }

fi  # wehave ssh


if wehave gpg-agent; then
    gpgagent () {
        local envfile
        export GPG_AGENT_INFO

        if [[ $OSTYPE == darwin* ]]; then
            # launchd or MacGPG2 might have already created a gpg-agent, and this
            # is the path it would use.  In any case, on OSX my $HOME is not on a
            # networked file system so I don't need to put the hostname in the
            # file.
            envfile=${HOME}/.gpg-agent-info
        else
            # $HOME might be on AFS so put the hostname in the file name.
            envfile=${HOME}/.gpg-agent-info-$(uname -n)
        fi
        local cmd='gpg-agent --daemon --write-env-file "$envfile"'

        if [[ -f $envfile ]]; then
            local sockpath
            # An entry looks like ``/tmp/gpg-InmhzI/S.gpg-agent:15169:1''
            # or ``/private/tmp/com.apple.launchd.F270SmYmCO/Listeners_agent:598:1''
            # where the actual socket path is all the stuff before the first `:'.
            sockpath=$(grep GPG_AGENT_INFO $envfile \
                        | cut -d= -f2 \
                        | cut -d: -f1)
            if [[ ! -S $sockpath ]]; then
                eval $cmd
            fi
        else
            eval $cmd
        fi
        source "$envfile"
    }

fi  # wehave gpg-agent





###############################################################################
# tmux
###############################################################################
if wehave byobu-tmux; then
    byobu_socket () {
        byobu-tmux -L "${1?Need socket}"
    }
    alias byobu-mosh='DISPLAY= byobu_socket MOSH'
fi

if wehave tmux && wehave tmux_socket; then
    alias tmux-iterm='tmux_socket ITERM -CC'
    alias tmux-shared='DISPLAY= tmux_socket SHARED'
    alias tmux-iterm-shared='tmux_socket SHARED -CC'
    alias tmux-mosh='DISPLAY= tmux_socket MOSH'
fi



# from https://unix.stackexchange.com/a/396888
has_ancestor_mosh() {
    # pstree -s is not on RHEL 6
    if [[ $OSTYPE == darwin* || $OSTYPE == *bsd* ]]; then
        pstree -p "$1" | grep -Fq mosh-server
        return
    else
        pstree -p | grep "$1" | grep -Fq mosh-server
        return
    fi
}

is_mosh() {
    local VERBOSE=false
    for arg in "$@"; do
        case $arg in
          -v|--verbose) VERBOSE=true ;;
        esac
    done

    local pid mosh_found
    if [[ -n ${TMUX-} ]]; then
        # current shell is under tmux
        local tmux_current_session tmux_client_id
        tmux_current_session=$(tmux display-message -p '#S')
        tmux_client_id=$(tmux list-clients -t "${tmux_current_session}" -F '#{client_pid}')
        # echo $tmux_current_session $tmux_client_id
        pid=$tmux_client_id
    else
        pid=$$
    fi

    mosh_found=$(has_ancestor_mosh $pid)   # or empty if not found
    if [[ -z $mosh_found ]]; then
        return 1     # exit code 1: not mosh
    fi

    $VERBOSE && echo mosh
    return 0         # exit code 0: is mosh
}

if [[ -n $DISPLAY ]] && wehave pstree && is_mosh; then
    unset DISPLAY
fi


###############################################################################
# END
###############################################################################

# hostname -f is slow when the network is shitty
full_hostname=$(uname -n)
short_hostname=${full_hostname%%.*}
domain_name=${full_hostname#*.}


for file in \
    "${DOTDIR}/.shell/$domain_name/advrc.sh" \
    "${DOTDIR}/.shell/$short_hostname/advrc.sh" \
    "${DOTDIR}/repos/newutils/public/shellfn/shlog.sh"
do
    [[ -r $file ]] && . "$file"
done
if [[ $full_hostname != $short_hostname && -r "${DOTDIR}/.shell/$full_hostname/advrc.sh" ]]; then
    . "${DOTDIR}/.shell/$full_hostname/advrc.sh"
fi

# vim:ft=bash
