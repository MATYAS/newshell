###############################################################################
# Environment functions
###############################################################################

# Usage: indirect_expand PATH -> $PATH
indirect_expand () {
    local IFS=''
    eval echo \$"{$1-}"
}

# Usage: pathremove /path/to/bin [PATH]
# Eg, to remove ~/bin from $PATH
#     pathremove ~/bin PATH
pathremove () {
    local IFS=':'
    local newpath
    local dir
    local var=${2:-PATH}
    # Bash has ${!var}, but this is not portable.
    for dir in `indirect_expand "$var"`; do
        IFS=''
        if [ "$dir" != "${1-}" ]; then
            newpath=$newpath:$dir
        fi
    done
    export $var=${newpath#:}
}

# Usage: pathprepend /path/to/bin [PATH]
# Eg, to prepend ~/bin to $PATH
#     pathprepend ~/bin PATH
pathprepend () {
    local var="${2:-PATH}"
    local value=`indirect_expand "$var"`
    export ${var}="${1-}${value:+:${value}}"
}

# Usage: pathappend /path/to/bin [PATH]
# Eg, to append ~/bin to $PATH
#     pathappend ~/bin PATH
pathappend () {
    local var=${2:-PATH}
    local value=`indirect_expand "$var"`
    export $var="${value:+${value}:}${1-}"
}

# Usage: ssource filename
ssource () {
    if [ -r "${1-}" ]; then
        local _oldopts=$-
        set +u # unset nounset
        set +C # unset noclobber
        . "${1-}"
        expr $_oldopts : '.*u' > /dev/null && set -u
        expr $_oldopts : '.*C' > /dev/null && set -C
    fi
}

# Because OS X does not do `hostname -d`
hostname_d () {
    local hostname
    hostname=`uname -n`
    echo ${hostname#*.}
}

###############################################################################
# for all shells, login and non-login
###############################################################################

DOTDIR=${DOTDIR:-$HOME}
export DOTDIR


####### Handling paths

PATH=$DOTDIR/bin:$DOTDIR/bin/utils/tiny:$DOTDIR/bin/utils:$DOTDIR/bin/py:$DOTDIR/go/bin:$DOTDIR/.local/bin:$PATH
if test -d /s/std/bin && test CentOS = "`lsb_release -is`" 2>/dev/null; then
    # ^ test if we're on a lab-supported machine versus just having AFS mounted
    PATH=$PATH:/s/std/bin
fi
PATH=$PATH:/usr/local/bin:/usr/local/sbin:/usr/bin:/bin:/usr/sbin:/sbin

is_login=${is_login:=false}
# Detect which shell we're running
# If this isn't accurate enough, I can do tricky things with /proc/$$ ...
if [ X${BASH-} != X ]; then
    SHELLTYPE=bash
    if shopt -q login_shell &> /dev/null; then
        is_login=true
    fi
elif [ X${ZSH_NAME-} != X ]; then
    SHELLTYPE=zsh
    if [[ -o login ]]; then
        is_login=true
    fi
else
    SHELLTYPE=sh
fi

if [ zsh = $SHELLTYPE ] || [ bash = $SHELLTYPE ]; then
    wehave () {
        hash "$@" &> /dev/null
    }
else
    wehave () {
        command -v "$@" &> /dev/null
    }
fi


###############################################################################
# login shells only
###############################################################################

if $is_login; then

    test -f "$DOTDIR/.pythonrc.py" && \
        export PYTHONSTARTUP="$DOTDIR/.pythonrc.py"

    export GOPATH
    : "${GOPATH:=$DOTDIR/go}"

    ##### Options for various apps
    export LESS="--search-skip-screen --LONG-PROMPT --QUIET --RAW-CONTROL-CHARS --ignore-case --chop-long-lines"
    export MALLOC_CHECK_=0
    # ^ needed to shut up the *** glibc detected *** errors
    export PYTHONDONTWRITEBYTECODE=1
    export VERSION_CONTROL="numbered"
    export XEDITOR="gvim"
    export XSESSION=~/.xsession

    ##### Personal variables
    export m=$HOME
    if test root = `id -un`; then
        if id -u me &> /dev/null; then
            export m=~me
        elif id -u ms &> /dev/null; then
            export m=~ms
        elif id -u mat &> /dev/null; then
            export m=~mat
        elif id -u matyas &> /dev/null; then
            export m=~matyas
        elif id -u vagrant &> /dev/null; then
            export m=~vagrant
        fi
    fi
    export md=$m/download
    export mshm=$m/shm
    export matmoria=matyas@moria.cs.wisc.edu
    export matph=$matmoria:public/html
    export matsite=https://pages.cs.wisc.edu/~matyas
    export mysite=https://pages.cs.wisc.edu/~matyas

    export fnaldn='/DC=org/DC=cilogon/C=US/O=Fermi National Accelerator Laboratory/OU=People/CN=Matyas Selmeci/CN=UID:matyas'
    export osgdn='/DC=org/DC=cilogon/C=US/O=University of Wisconsin-Madison/CN=Matyas Selmeci A148276'

    export       ghme=matyasselmeci
    export      ghosg=opensciencegrid
    export     ghosg2=osg-htc
    export     harbor=hub.opensciencegrid.org
    export   harborme=$harbor/matyasosg
    export  harborosg=$harbor/$ghosg
    export harborosg2=$harbor/$ghosg2

    ## Locale stuff
    #if locale -a | fgrep -qx C.utf8; then
    #    clocale=C.utf8
    #else
    #    clocale=C
    #fi
    export LANG=en_US.utf8




    # Default XDG config values according to http://standards.freedesktop.org/basedir-spec/basedir-spec-0.6.html
    : "${XDG_DATA_HOME:=$HOME/.local/share}"
    : "${XDG_CONFIG_HOME:=$HOME/.config}"
    : "${XDG_DATA_DIRS:=/usr/local/share:/usr/share/}"
    : "${XDG_CONFIG_DIRS:=/etc/xdg}"
    : "${XDG_CACHE_HOME:=$HOME/.cache}"
    export XDG_DATA_HOME XDG_CONFIG_HOME XDG_DATA_DIRS XDG_CONFIG_DIRS XDG_CACHE_HOME


    umask 022

fi
################################################################################
# end of login shell only
################################################################################

if wehave qvim; then
    EDITOR=qvim
    unset SVN_EDITOR
else
    export SVN_EDITOR=vi
fi

if wehave kak; then
    VISUAL=kak
elif wehave vim; then
    VISUAL=vim
elif wehave vi; then
    VISUAL=vi
elif wehave nano; then
    VISUAL=nano
fi
EDITOR=${EDITOR:-${VISUAL:-ed}}
export EDITOR VISUAL

# kill shell after ten days with no input
TMOUT=864000


# hostname -f is slow on Fedora 25
full_hostname=$(uname -n)
short_hostname=${full_hostname%%.*}
domain_name=${full_hostname#*.}

for file in \
    "$DOTDIR/.shell/$domain_name/profile.sh" \
    "$DOTDIR/.shell/$short_hostname/profile.sh"
do
    [ -r "$file" ] && . "$file"
done
if [ "$full_hostname" != "$short_hostname" ] && [ -r "$DOTDIR/.shell/$full_hostname/profile.sh" ]; then
    . "$DOTDIR/.shell/$full_hostname/profile.sh"
fi

if [ "X${VIRTUAL_ENV-}" != X ] && [ -d "${VIRTUAL_ENV-}/bin" ]; then
    pathprepend "${VIRTUAL_ENV-}/bin"
fi

if wehave perl; then
    # deduplicate $PATH
    PATH=`
    perl -le '
    for $it (split /:/, $ARGV[0]) {
        if (!exists($s{$it})) { $s{$it}=1; push @l, $it; }
    }
    $"=":"; print "@l";
    ' "$PATH"
    `
fi


# vim:ft=sh
