from __future__ import absolute_import, print_function
import math, os, re, sys  # pylint: disable=unused-import
from collections import (  # pylint: disable=unused-import
    defaultdict,
    namedtuple,
    OrderedDict,
)


PY2 = sys.version_info[0] == 2
PY3 = not PY2


def can_import(modulestr):
    # type: (str) -> bool
    """Return if a module can be imported without actually importing it.
    Only works with top-level modules (not dotted modules)
    Credit: https://stackoverflow.com/a/14050282
    """
    if PY2:
        import pkgutil  # type: ignore[import]

        return pkgutil.find_loader(modulestr) is not None
    elif sys.version_info < (3, 4):
        import importlib  # type: ignore[import]

        return importlib.find_loader(modulestr) is not None
    else:
        import importlib.util  # type: ignore[import]

        return importlib.util.find_spec(modulestr) is not None


# https://stackoverflow.com/questions/4984647/accessing-dict-keys-like-an-attribute
class lazydict(dict):
    """
    dict whose attributes can be accessed via dot notation.

    """

    def __getattr__(self, attr):
        return self[attr]

    def __setattr__(self, attr, value):
        self[attr] = value


class __missing(object):
    """
    A singleton that is returned whenever a missing key is accessed.
    Considered false and does nothing when accessed via various methods.
    """

    __slots__ = ()

    def __str__(self):
        return "Missing"

    def __bytes__(self):
        return b"Missing"

    def __bool__(self):
        return False

    __nonzero__ = __bool__  # python 2

    def __repr__(self):
        return "Missing"

    def __getattr__(self, _):
        return self

    def __setattr__(self, _, __):
        pass

    def __call__(self, *_, **__):
        return self

    def __getitem__(self, _):
        return self

    def __setitem__(self, _, __):
        pass

    def __contains__(self, _):
        return False


Missing = __missing()


class safelazydict(dict):
    """
    dict whose attributes can be accessed via dot notation, and uses a "Missing" object instead of returning None or raising an error.
    """

    def __getitem__(self, item):
        return self.get(item, Missing)

    def __getattr__(self, attr):
        return self.get(attr, Missing)

    def __setattr__(self, attr, value):
        self[attr] = value

    ### overriding __repr__ messes up pretty-printing
    # def __repr__(self):
    #     return "%s(%r)" % (self.__class__.__name__, dict(self))


def to_sld(orig):
    """Convert all dicts in a nested data structure into safelazydicts"""
    # TODO: Handle other dict- and list-like types like OrderedDict
    if isinstance(orig, dict) and not isinstance(orig, safelazydict):
        new_orig = safelazydict()
        for k, v in orig.items():
            new_orig[k] = to_sld(v)
        return new_orig
    elif isinstance(orig, tuple):
        return tuple(to_sld(it) for it in orig)
    elif isinstance(orig, list):
        return list(to_sld(it) for it in orig)
    else:
        return orig


def un_sld(orig):
    """Convert all safelazydicts in a nested data structure into regular dicts"""
    if isinstance(orig, dict):  # safelazydict is a subclass of dict
        if not isinstance(orig, safelazydict):
            new_orig = orig.__class__()
        else:
            new_orig = dict()
        for k, v in orig.items():
            if v is not Missing:
                new_orig[k] = un_sld(v)
        return new_orig
    elif isinstance(orig, tuple):
        return tuple(un_sld(it) for it in orig if it is not Missing)
    elif isinstance(orig, list):
        return list(un_sld(it) for it in orig if it is not Missing)
    else:
        return orig


if PY3:

    # https://stackoverflow.com/a/41658338
    def execfile(filename, globalvars=None, localvars=None):
        """Read and execute a Python script from a file.
        The globals and locals are dictionaries, defaulting to the current
        globals and locals.  If only globals is given, locals defaults to it.

        This is a reimplementation of Python 2's execfile(...) for Python 3.
        """
        if globalvars is None:
            globalvars = globals()

        has_file = "__file__" in globalvars
        has_name = "__name__" in globalvars

        oldfile = globalvars.get("__file__", None)
        oldname = globalvars.get("__name__", None)

        globalvars.update({"__file__": filename, "__name__": "__main__"})
        with open(filename, "rb") as f:
            code = compile(f.read(), filename, "exec")
            exec(code, globalvars, localvars)  # pylint: disable=exec-used

        if has_file:
            globalvars["__file__"] = oldfile
        else:
            del globalvars["__file__"]

        if has_name:
            globalvars["__name__"] = oldname
        else:
            del globalvars["__name__"]


try:
    from shutil import which as _which
except ImportError:
    from distutils.spawn import find_executable as _which


__editor = None


def _get_editor():
    global __editor

    if __editor:
        return __editor

    for it in [
        os.environ.get("VISUAL"),
        os.environ.get("EDITOR"),
        "vim",
        "joe",
        "vi",
        "nano",
        "ed",
    ]:
        if not it:
            continue
        editor = _which(it)
        if editor:
            __editor = editor
            return editor


def editandreturn(initial="", suffix=""):
    """Put some text in a temporary file then open the file in an editor and read back the results.

    Parameters
    ----------
    initial : str optional
        The initial text to put in the file.
    suffix : str optional
        A suffix to give the file (e.g. for syntax highlighting).

    Returns
    -------
    str
        The modified text.

    """
    import tempfile
    import subprocess

    # Had problems with NamedTemporaryFile in OSX:
    # flushing, seeking, and rereading the file contents did not
    # give me the new edited contents.
    fd, name = tempfile.mkstemp(suffix=suffix)
    try:
        os.write(fd, initial.encode())
        os.fsync(fd)
        subprocess.check_call([_get_editor(), name])
        with open(name) as fh:
            return fh.read()
    finally:
        os.close(fd)  # can't unlink before close on windows
        os.unlink(name)


def editandexec(initial="", globalvars=None, localvars=None):
    """Put some text a temporary Python file, open the file in an editor, then exec the modified results.

    Parameters
    ----------
    initial : str optional
        The initial text to put in the file.

    """
    if globalvars is None:
        globalvars = globals()

    import tempfile
    import subprocess

    # See editandreturn for why NamedTemporaryFile didn't work
    fd, name = tempfile.mkstemp(suffix=".py")
    try:
        os.write(fd, initial.encode())
        os.fsync(fd)
        subprocess.check_call([_get_editor(), name])
        return execfile(name, globalvars, localvars)
    finally:
        os.close(fd)  # can't unlink before close on windows
        os.unlink(name)


def editasyaml(data):
    """Print data as yaml, open it in an editor, let the user edit it, then reload it.

    Parameters
    ----------
    data : object
        Anything that can be dumped into yaml format

    Returns
    -------
    object
        The modified data

    """
    import yaml

    dump = getattr(yaml, "unsafe_dump", yaml.dump)
    load = getattr(yaml, "unsafe_load", yaml.load)

    import tempfile
    import subprocess

    # See editandreturn for why NamedTemporaryFile didn't work
    fd, name = tempfile.mkstemp(suffix=".yaml")
    fh = os.fdopen(fd, "w")
    try:
        dump(data, fh)
        os.fsync(fd)
        subprocess.check_call([_get_editor(), name])
        with open(name) as fh:
            return load(fh)
    finally:
        fh.close()  # can't unlink before close on windows
        os.unlink(name)


def editasjson(data):
    """Print data as json, open it in an editor, let the user edit it, then reload it.

    Parameters
    ----------
    data : object
        Anything that can be dumped into json format

    Returns
    -------
    object
        The modified data

    """
    import json

    return json.loads(
        editandreturn(json.dumps(data, sort_keys=True, indent=4), suffix=".json")
    )


def editastoml(data):
    """Print data as toml, open it in an editor, let the user edit it, then reload it.

    Parameters
    ----------
    data : object
        Anything that can be dumped into toml format

    Returns
    -------
    object
        The modified data

    """
    import toml

    return toml.loads(editandreturn(toml.dumps(data), suffix=".toml"))


def evalinput(prompt="-> ", noliteral=False):
    """Read input from the command line then eval the result.  Returns the raw
    string in case of a syntax error (unless noliteral is set).  Asks the user
    to try again in case of other errors (except SystemExit).

    Parameters
    ----------
    prompt : str optional
        The prompt to display.
    noliteral : bool optional
        Default False. If True, will ask the user to try again on NameError and SyntaxError instead of returning the raw string.

    """
    if not hasattr(__builtins__, "raw_input"):  # py3
        raw_input = __builtins__.input

    while True:
        x = raw_input(prompt)
        try:
            return eval(x)  # pylint: disable=eval-used
        except SystemExit:  # pylint: disable=try-except-raise
            raise
        except (SyntaxError, NameError) as e:
            if noliteral:
                print(e)
                print("try again")
                continue
            return x
        except Exception as e:  # pylint: disable=broad-except
            print(e)
            print("try again")
            continue


def dirpublic(obj):
    """dir(obj) with the non-public (i.e. starts with `_') elements
    filtered out.

    """
    return [x for x in dir(obj) if not x.startswith("_")]


def pp(obj, *args, **kwargs):
    """Pretty-print an object; see pprint.pprint(). Use rich.print() if available"""
    if hasattr(globals().get("rich"), "print"):
        return rich.print(un_sld(obj))
    else:
        import pprint

        kwargs.setdefault("width", int(os.environ.get("COLUMNS", 80)))
        return pprint.pprint(un_sld(obj), *args, **kwargs)


ATOZ = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
atoz = "abcdefghijklmnopqrstuvwxyz"


if str is bytes:  # Python 2

    def to_str(strlike, encoding="utf-8", errors="backslashreplace"):
        """Turns a unicode into a str (bytes) or leaves it alone.

        The default encoding is utf-8 (which will not raise
        a UnicodeEncodeError); you may have gotten unicode from json.loads().
        """
        if isinstance(strlike, unicode):
            return strlike.encode(encoding, errors)
        return strlike

    to_bytes = to_str

    def asciiupper(strlike):
        """Uppercase just the ASCII range (i.e. [a-z]) of a str or unicode"""
        import string

        if isinstance(strlike, unicode):
            return strlike.translate(string.maketrans(atoz, ATOZ).decode("latin-1"))
        else:
            return strlike.upper()

    def asciilower(strlike):
        """Lowercase just the ASCII range (i.e. [A-Z]) of a str or unicode"""
        import string

        if isinstance(strlike, unicode):
            return strlike.translate(string.maketrans(ATOZ, atoz).decode("latin-1"))
        else:
            return strlike.lower()

else:  # Python 3

    def to_str(strlike, encoding="latin-1", errors="strict"):
        """Turns a bytes into a str or leaves it alone.

        The default encoding is latin-1 (which will not raise
        a UnicodeDecodeError); best to use when you want to treat the data
        as arbitrary bytes, but some function is expecting a str.
        """
        if isinstance(strlike, bytes):
            return strlike.decode(encoding, errors)
        return strlike

    def to_bytes(strlike, encoding="latin-1", errors="backslashreplace"):
        """Turns a str into bytes or leaves it alone.

        The default encoding is latin-1 under the assumption that you have
        obtained the str from to_str, applied some transformation, and want
        to pass it back to the system.
        """
        if isinstance(strlike, str):
            return strlike.encode(encoding, errors)
        return strlike

    def asciiupper(strlike):
        """Uppercase just the ASCII range (i.e. [a-z]) of a str or bytes"""
        if isinstance(strlike, bytes):
            return strlike.upper()
        else:
            return strlike.translate(str.maketrans(atoz, ATOZ))

    def asciilower(strlike):
        """Lowercase just the ASCII range (i.e. [A-Z]) of a str or bytes"""
        if isinstance(strlike, bytes):
            return strlike.lower()
        else:
            return strlike.translate(str.maketrans(ATOZ, atoz))


def safe_get(x, *keys, **kwargs):
    # ^^ Python 2 doesn't allow keyword arguments after *args so do it by hand
    default = kwargs.pop("default", None)
    if kwargs:
        raise TypeError("safe_get() only accepts the 'default' kwarg")
    try:
        for key in keys:
            x = x[key]
        return x
    except (IndexError, KeyError, TypeError, AttributeError):
        return default


# Interactive mode only:

if __name__ == "__main__":
    _can_haz_ipython = False
    try:
        __IPYTHON__  # type: ignore[name-defined]
        _can_haz_ipython = True
    except NameError:
        if can_import("IPython"):
            _can_haz_ipython = True
            print("run ipyme() to start IPython")

            def ipyme(*a, **kwa):
                import IPython  # type: ignore[import]

                IPython.start_ipython(*a, **kwa)
                raise SystemExit

        else:
            print("no ipython for you")

    if can_import("rich"):
        print("we're rich!", end="")
        if _can_haz_ipython:
            print(" use `%load_ext rich` for goodies")
        else:
            print()

    if not can_import("six"):
        print("no six for you")

    if not hasattr(__builtins__, "unicode"):
        unicode = __builtins__.unicode = str

    try:
        import readline
        import rlcompleter  # pylint: disable=unused-import

        readline.parse_and_bind("tab: complete")
    except ImportError:
        pass
