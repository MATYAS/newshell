fish_default_key_bindings
#fish_vi_key_bindings

abbr g git

alias cp "cp -i"
alias mv "mv -i"
alias rm "rm -i"

alias del rm

alias back prevd

if test -z "$DOTDIR"
    set -x DOTDIR "$HOME"
end

set -x SHELLDIR "$DOTDIR/.shell"

set full_hostname (uname -n)
set short_hostname (string replace -r '^([^.]+).*$' '$1' "$full_hostname")
set domain_name (string replace -r '^[^.]+\.(.+)$' '$1' "$full_hostname")

if test "$short_hostname" = "yehat"
    set -p PATH "$DOTDIR/newutils/fun"
end

set -p PATH "$DOTDIR/bin" "$DOTDIR/bin/utils" "$DOTDIR/bin/py" "$DOTDIR/go/bin" "$DOTDIR/.local/bin" "$DOTDIR/newutils/public" "$DOTDIR/newutils/public/tiny"
set -a PATH "/usr/local/bin" "/usr/local/sbin" "/usr/bin" "/bin" "/usr/sbin" "/sbin"

set -x PYTHONSTARTUP "$DOTDIR/.pythonrc.py"

test -z "$XDG_DATA_HOME"; or set -x XDG_DATA_HOME "$HOME/.local/share"
test -z "$XDG_CONFIG_HOME"; or set -x XDG_CONFIG_HOME "$HOME/.config"
test -z "$XDG_DATA_DIRS"; or set -x XDG_DATA_DIRS "/usr/local/share:/usr/share"
test -z "$XDG_CONFIG_DIRS"; or set -x XDG_CONFIG_DIRS "/etc/xdg"
test -z "$XDG_CACHE_HOME"; or set -x XDG_CACHE_HOME "$HOME/.cache"

if command -s qvim >/dev/null
    set -x EDITOR qvim
    set -e SVN_EDITOR
else
    set -x SVN_EDITOR vi
end

if command -s kak >/dev/null
    set -x VISUAL kak
else if command -s vim >/dev/null
    set -x VISUAL vim
end

if command -s kubectl >/dev/null
    source "$SHELLDIR/kubealiases.fish"
end



set -x ghme        "matyasselmeci"
set -x ghosg       "opensciencegrid"
set -x ghosg2      "osg-htc"
set -x harbor      "hub.opensciencegrid.org"
set -x harborosg   "$harbor/$ghosg"
set -x harborosg2  "$harbor/$ghosg2"
set -x harborme    "$harbor/matyasosg"



if test -r "$SHELLDIR/$domain_name/config.fish"
    source "$SHELLDIR/$domain_name/config.fish"
end
if test -r "$SHELLDIR/$short_hostname/config.fish"
    source "$SHELLDIR/$short_hostname/config.fish"
end
if test -r "$SHELLDIR/$full_hostname/config.fish"
    source "$SHELLDIR/$full_hostname/config.fish"
end
