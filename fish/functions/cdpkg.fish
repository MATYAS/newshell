function cdpkg -d "cd to a package directory under ~/native/redhat"
    set -l newdir
    if test -n $argv[1]
        set newdir (find -L ~/native/redhat/branches/* -maxdepth 2 -name '.*' -prune -o -name '_*' -prune -o -type d -print | fzf -1 -q "$argv[1]")
    else
        set newdir (find -L ~/native/redhat/branches/* -maxdepth 2 -name '.*' -prune -o -name '_*' -prune -o -type d -print | fzf)
    end
    if test -d "$newdir"
        cd "$newdir"; and ls
    end
end
