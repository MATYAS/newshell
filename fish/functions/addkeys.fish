function addkeys -d "Adds keys to the running ssh-agent"
    find $HOME/.ssh \
        -name "id_*" \
        -not -name "*pub*" \
        -not -name "*autossh*" \
        -exec ssh-add "{}" +
end
