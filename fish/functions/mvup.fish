function mvup
	set -l curdir (basename (pwd))
	cd ..
        # need 'set' to get nullglob behavior; if I inline $files into mv it would behave like failglob
	set -l files "$curdir"/* "$curdir"/.*
	and mv $files .
	and rmdir "$curdir"
end
