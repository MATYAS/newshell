function nofish
    setenv nofish 1
    set flags -i
    if test "$login" != ""
        set flags {$flags}l
    end
    if test "$oldshell" != ""; and command -s "$oldshell" >/dev/null
        exec "$oldshell" $flags
    else if command -s zsh >/dev/null
        exec zsh $flags
    else if command -s bash >/dev/null
        exec bash $flags
    else if command -s sh >/dev/null
        exec sh $flags
    else
        echo "can't find a shell wtf"
        return 1
    end
end
