function sshagent -d "Starts or loads an ssh-agent"
    set -l agentfile $HOME/.ssh/(hostname -s)-agent.csh
    set -l num_ssh_agents (ps -u $USER | grep -c '[s]sh-agent')
    if test \( 1 -gt "$num_ssh_agents" \) -o \( \! -e "$agentfile" \) 
        command ssh-agent -c > $agentfile
    end
    source $agentfile
end
