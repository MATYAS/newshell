function c
    if test -z "$argv[1]"
        cd; and ls
    else if test -d "$argv[1]"
        cd "$argv[1]"; and ls
    else
        set -l dirs $argv[1]*
        if test 1 -eq (count $dirs)
            cd "$dirs"; and ls
        else if test 0 -eq (count $dirs)
            echo "no such directory"
            return 1
        else
            set -l escdirs (string escape $dirs)
            set -l newdir (sh -c "select it in $escdirs; do echo \$it; break; done")
            cd $newdir; and ls
        end
    end
end
