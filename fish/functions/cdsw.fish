function cdsw
    set -l newdir
    if test -n $argv[1]
        set newdir (find -L ~/workrepos/ ~/repos2/ -maxdepth 1 -name '.*' -prune -o -name '_*' -prune -o -type d -print | fzf -1 -q "$argv[1]")
    else
        set newdir (find -L ~/workrepos/ ~/repos2/ -maxdepth 1 -name '.*' -prune -o -name '_*' -prune -o -type d -print | fzf)
    end
    if test -d "$newdir"
        cd "$newdir"; and ls
    end
end
