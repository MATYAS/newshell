
function __complete_kustomize
    set -lx COMP_LINE (commandline -cp)
    test -z (commandline -ct)
    and set COMP_LINE "$COMP_LINE "
    (which kustomize)
end
command -v kustomize >/dev/null; and complete -f -c kustomize -a "(__complete_kustomize)"

