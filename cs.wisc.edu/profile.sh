if $is_login && test ms != `id -un`; then
    export EMAIL="matyas@cs.wisc.edu"
fi
# hostname slow on Fedora 25
if test siren.cs.wisc.edu '!=' `uname -n`; then
    pathprepend "$HOME/sys/lib" LD_LIBRARY_PATH

    pathprepend "$HOME/sys/share/man" MANPATH
    pathappend "/s/std/man" MANPATH
    pathappend "/usr/share/man" MANPATH

    pathprepend "$HOME/sys/lib/perl5/lib/site_perl" PERLLIB

    pathprepend "$HOME/sys/lib/pkgconfig/" PKG_CONFIG_PATH

    pathprepend "/s/std/bin"

    pathprepend "$DOTDIR/repos/newutils/csl" PATH
    pathprepend "$DOTDIR/repos/newutils/fun" PATH
    pathprepend "$DOTDIR/repos/newutils/linux" PATH
    pathprepend "$DOTDIR/repos/newutils/public" PATH
    pathprepend "$DOTDIR/repos/newutils/public/tiny" PATH
    pathprepend "$DOTDIR/repos/newutils/unix" PATH

    pathprepend "/nobackup/matyas/software/bin"
    pathprepend "/nobackup/matyas/software/share/man" MANPATH
    pathprepend "/u/m/a/matyas/software/go/bin"
    export GOROOT=/u/m/a/matyas/software/go

    pathprepend "$HOME/sys/bin"
    pathprepend "$HOME/sys/share/man" MANPATH
    ## ^ Should be first (i.e. set last)

    pathappend "$HOME/osg/osg-client"
    pathappend "$HOME/sys/lib/perl5/bin"
    pathappend "/s/afstools-1/common/sbin"
    pathappend "/s/krb5/sbin"
    pathappend "/unsup/colordiff/bin"
    pathappend "/unsup/rlwrap/bin"
    pathappend "/usr/afsws/bin"
    pathappend "/usr/afsws/etc"

    export VISUAL=vim

    export TZ=America/Chicago

    #######
    # login shells only
    #######

    if $is_login; then
        test -d /dev/shm/matyas || ( umask 077; mkdir -p /dev/shm/matyas )
        test -d "$DOTDIR/shm" && export TMPPATH=$DOTDIR/shm
        if [ "$KRB5CCNAME" = "FILE:*" ]; then
            /s/std/bin/stashticket
            mkdir -m 0700 -p /dev/shm/matyas/krb
            cp -p "${KRB5CCNAME#FILE:}" /dev/shm/matyas/krb/tkt
            export KRB5CCNAME=DIR:/dev/shm/matyas/krb
        fi




        export PERL5LIB="$PERLLIB"

        export PRINTER=copier-4b
        export RUBYOPTS="-W0"
        ## ^ don't warn me about insecure world-writable dir




        ##### More personal variables

        ### Special dirs
        export m=$HOME
        export md=$m/download
        export mshm=$m/shm
        export ob=$HOME/nn/ob
        export soft=/nobackup/matyas/software
        ### VDT
        export vdtwww=/p/vdt/public/html
        export vs=file:///p/vdt/workspace/svn
        export vdtsvn=file:///p/vdt/workspace/svn
        export vdtgit=/p/condor/workspaces/vdt/git

        export pcvw=/p/condor/workspaces/vdt
        export pcw=/p/condor/workspaces
        export pcwv=/p/condor/workspaces/vdt
        export pcwm=/p/condor/workspaces/matyas
        export vdtu=$vdtwww/upstream
    fi
    #######
    # end of login shell only
    #######

fi

if test -d /dev/shm && ! test -d /dev/shm/matyas; then
    (umask 077; mkdir -p /dev/shm/matyas)
fi

# vim:ft=sh
