if type -f abbrev-alias &>/dev/null; then
    abbrev-alias ok='osg-koji'
    abbrev-alias koji='osg-koji'
    abbrev-alias catcert='openssl x509 -in'
fi

