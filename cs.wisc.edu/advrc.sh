alias ok='osg-koji'
alias koji='osg-koji'
alias fedorakoji='osg-koji --noauth -c ~/nn/fedora-koji.conf'
alias klt='koji_list_tagged'
alias ktir="osg-koji taskinfo -r"
alias kfnal="kinit -f matyas@FNAL.GOV"

alias cabali="cabal install --prefix=$soft/stow/cabal"
alias sshaya="ssh localhost -p 10002"
alias vncaya="vncviewer localhost::10003"
alias moshaya="mosh -p 60002 --ssh='ssh -p 10002' 127.0.0.1"

alias cdnoafs='cd ${PWD#/afs/cs.wisc.edu}'
alias cdphysnoafs='cdphys; cdnoafs'

alias myksu="ksu -e /usr/bin/env DOTDIR=/u/m/a/matyas ZDOTDIR=/u/m/a/matyas $SHELL"

alias shorttag="resolve_short_tag"
alias gterm="gnome-terminal"

alias fcl='fcl '
alias xfcl='xrun fcl '

alias tclsh='rlwrap tclsh'
alias wish='rlwrap wish'

alias catcert="openssl x509 -in"

get_proxy_if_needed () {
    if `wehave grid-proxy-info` && `wehave grid-proxy-init`; then
        grid-proxy-info -e || grid-proxy-init -bits 2048 -rfc
    elif `wehave osgrun`; then
        osgrun grid-proxy-info -e || osgrun grid-proxy-init -bits 2048 -rfc
    else
        echo "Can't find grid-proxy-info or grid-proxy-init" >&2
        exit -1
    fi
}

koji_list_tagged () {
    local tagshort
    if [[ $1 == '-s' ]]; then
        tagshort=$(koji-resolveshorttag $2)
        [[ $? -ne 0 ]] && return 1
        shift 2
        osg-koji -q list-tagged $tagshort "$@"
    else
        osg-koji -q list-tagged "$@"
    fi
}

_getpkgdir () {
    # Guess the package directory using the following logic:
    #   If we're in a dir called 'osg/' or 'upstream/', it's the parent dir.
    #   If we're under one of the underscore directories, it's the parent of the underscore dir.
    #     (that is, if we're in 'foo/_quilt/bar/narf/poit', then guess 'foo')
    #   Otherwise, guess $PWD.
    # Not very robust. Does not know when to err out.
    local guess
    if [[ ${PWD##*/} == 'osg' || ${PWD##*/} == 'upstream' ]]; then
        echo ${PWD%/*}
    else
        for udir in _final_srpm_contents _upstream_srpm_contents _build_results _upstream_tarball_contents _quilt _git; do
            guess=${PWD%%${udir}*}
            if [[ $guess != $PWD ]]; then
                echo $guess
                return 0
            fi
        done
        echo $PWD
    fi
}

_getpatchesdir () {
    # Look for a directory named patches in either the current directory or
    # the parents.
    (
        while [[ $PWD != / ]]; do
            if [[ -d patches ]]; then
                echo $PWD/patches
                return 0
            else
                cd ..
            fi
        done
    )
}


uppatch () {
    # Upload a named patch (with or without the .patch or .diff suffix) to the osg/ dir
    local patchfile patchbase destdir
    if [[ -z ${1-} ]]; then
        patchfile=`quilt top` || { echo "Patch name not provided and cannot be determined via 'quilt top'"; return 1; }
        if [[ ! -e $patchfile ]]; then
            patchfile=$(_getpatchesdir)/$patchfile
            if [[ ! -e $patchfile ]]; then
                echo "Could not find patch file"
                return 1
            fi
        fi
    else
        patchbase=${1%.patch}
        patchbase=${patchbase%.diff}
        if [[ -e $1 ]]; then
            patchfile=$1
        elif [[ -e patches/$1 ]]; then
            patchfile=patches/$1
        elif [[ -e patches/$patchbase.patch ]]; then
            patchfile=patches/$patchbase.patch
        elif [[ -e patches/$patchbase.diff ]]; then
            patchfile=patches/$patchbase.diff
        else
            echo "Cannot find matching patch"
            return 1
        fi
    fi
    destdir=$(_getpkgdir)/osg/
    if [[ ! -d "$destdir" ]]; then
        echo Guessing dest dir is "'$destdir'" but it does not exist.
        if ask_yn "Create it (y/n)?"; then
            mkdir -p "$destdir"
        else
            echo Cannot find dest dir
            return 1
        fi
    fi
    cp -vf "$patchfile" "$destdir"
}


upgitpatch () {
    local root patch
    root=$(git root) || return 1
    patch=$(git fp1) || return 1
    (cd "$root" && uppatch "$patch")
}


_getpkgspec () {
    local specfiles
    if [[ -z "${1:-}" ]]; then
        specfiles="$(_getpkgdir)/osg/*.spec"
    elif [[ -d $1 ]]; then
        if [[ $(basename $1) == 'osg' ]]; then
            specfiles="*.spec"
        else
            specfiles="${1}/osg/*.spec"
        fi
    elif [[ -f $1 ]]; then
        specfiles="${1}"
    else
        specfiles="${1}*/osg/*.spec"
    fi
    echo "$specfiles"
}

specedit () {
    # Edit the specfile of the current package
    local specfiles
    specfiles=$(_getpkgspec "$@")
    # hack
    local editor=${EDITOR:-vim}
    if [[ $editor == "vi" ]]; then
        editor=vim
    fi
    eval "_edit_1 $editor $specfiles"
}

vspecedit () {
    EDITOR="vim" specedit "$@"
}

gspecedit () {
    EDITOR="gvim" specedit "$@" &> /dev/null
}

specview () {
    EDITOR="less" VISUAL="vim" specedit "$@"
}

extract_upstream () {
    : ${vdtu?Do not know where upstream dir is}
    local sourcefiles
    if [[ -z "${1:-}" ]]; then
        sourcefiles="$(_getpkgdir)/upstream/*.source"
    elif [[ -d $1 ]]; then
        if [[ ${1##*/} == 'upstream' ]]; then
            sourcefiles="*.source"
        else
            sourcefiles="${1}/upstream/*.source"
        fi
    elif [[ -f $1 ]]; then
        sourcefiles="$1"
    else
        echo "Cannot find source files"
        return 1
    fi
    for sfile in `eval echo $sourcefiles`; do
        if [[ -f $sfile ]]; then
            grep -v '^#' $sfile | while read line; do
                extract $vdtu/$line
            done
        fi
    done
}

extract_upstream_spec () {
    : ${vdtu?Do not know where upstream dir is}
    local -a sourcefiles
    if [[ -z "${1:-}" ]]; then
        sourcefiles=( $(_getpkgdir)/upstream/*.source )
    elif [[ -d $1 ]]; then
        if [[ ${1##*/} == upstream ]]; then
            sourcefiles=( *.source )
        else
            sourcefiles=( ${1}/upstream/*.source )
        fi
    elif [[ -f $1 ]]; then
        sourcefiles=( $1 )
    else
        echo "Cannot find source files"
        return 1
    fi
    for sfile in "${sourcefiles[@]}"; do
        if [[ -f $sfile ]]; then
            grep -v '^#' $sfile | while read line; do
                if [[ $line = *.src.rpm ]]; then
                    rpm2cpio "$vdtu/$line" | cpio -ivd '*.spec'
                fi
            done
        fi
    done
}

kojiwaitrepo () {
    local buildnodver=${1%.el[56]}
    local build
    local dver
    for dver in el6 el7; do
        build=${buildnodver}.${dver}
        if ! osg-koji buildinfo $build | grep 'No such build'; then
            osg-koji wait-repo ${dver}-osg-build --build=$build
        fi
    done
}

promote () {
    local executable
    executable=`which osg-promote 2> /dev/null` || executable=$ob/osg-promote
    [[ -x $executable ]] || { echo "Can't find osg-promote in path or at $executable"; return 1; }
    $executable "$@"
}

alias promote36="promote -r 3.6-testing"
alias promote35="promote -r 3.5-testing"
alias promote35up="promote -r 3.5-upcoming"
alias promote36up="promote -r 3.6-upcoming"
alias promoteboth="promote -r 3.6-testing -r 3.5-testing"

promoterequest36 () {
    echo "*Builds*"
    koji-build-table --tag=osg-3.6-el{7,8}-development "$@" || return $?
    echo
    echo "Permission to promote the above builds to testing?"
}

promoterequest35 () {
    echo "*Builds*"
    koji-build-table --tag=osg-3.5-el{7,8}-development "$@" || return $?
    echo
    echo "Permission to promote the above builds to testing?"
}

promoterequest35up () {
    echo "*Builds*"
    koji-build-table --tag=osg-3.5-upcoming-el{7,8}-development "$@" || return $?
    echo
    echo "Permission to promote the above builds to 3.5-upcoming-testing?"
}

promoterequest36up () {
    echo "*Builds*"
    koji-build-table --tag=osg-3.6-upcoming-el{7,8}-development "$@" || return $?
    echo
    echo "Permission to promote the above builds to 3.6-upcoming-testing?"
}

promoterequestboth () {
    echo "*Builds*"
    koji-build-table --tag=osg-3.6-el{8,7}-development --tag=osg-3.5-el{8,7}-development "$@" || return $?
    echo
    echo "Permission to promote the above builds to testing?"
}

sshfc () {
    (
        ssource $HOME/private/fcl.env
        if klist | grep -q 'Default principal.*FNAL.GOV'; then
            local whichmachine=$1
            local user=$2
            local list choices
            local shorthost hostname
            if [[ $whichmachine =~ ^[0-9]+ ]]; then
                hostname=$(printf "fermicloud%03d.fnal.gov" ${whichmachine%%.*})
            else
                list=$(fcl -l | fgrep $whichmachine)
                echo "$list"
                choices=$(awk '{print $3}' <<<"$list")
                select shorthost in $choices; do
                    hostname=${shorthost}.fnal.gov
                    break
                done
            fi

            settitle ${user[1]}\@${whichmachine}
            shift 2
            if [[ $TERM = screen*-it || $TERM = tmux* ]]; then
                TERM=screen
            elif [[ $TERM = xterm-kitty ]]; then
                TERM=xterm-256color
            fi
            export TERM
            ssh -o ProxyCommand="ssh -qW %h:%p openstackuigpvm01.fnal.gov" \
                -4 \
                -o StrictHostKeyChecking=no \
                -o UserKnownHostsFile=/dev/null \
                -K \
                $hostname \
                -l $user \
                "$@"
            # ^ -K forwards the ticket
            #   -4 forces IPv4 (I was having port forwarding problems without it ("bind: cannot assign requested address"))
            settitle $SHELL
        else
            echo 'No ticket'
            return 1
        fi
    )
}

sshfcr () {
    local nad=$1
    shift 1
    sshfc $nad root "$@"
}

sshfcm () {
    local nad=$1
    shift 1
    sshfc $nad matyas "$@"
}

sshfcl () {
    select dest in $(fcl -l | awk '{print $5}'); do
        case $dest in
            [0-9]*) sshfcr $dest; return $? ;;
            *) echo "doesn't look like the name of a vm"; return 1 ;;
        esac
    done
}

scpupfc () {
    (
        ssource $HOME/shm/fcl.env
        if klist | grep -q 'Default principal.*FNAL.GOV'; then
            local machine=$(printf "fermicloud%03d.fnal.gov" $1)
            shift 1
            scp -o ProxyCommand="ssh -qW %h:%p openstackuigpvm01.fnal.gov" -o StrictHostKeyChecking=no "$@" $machine:
        else
            echo 'No ticket'
            return 1
        fi
    )
}

gitpushfcl () {
    (
        ssource $HOME/shm/fcl.env
        mirror=$(git config --local --get remote.fcl.mirror)
        if [[ $mirror = true ]]; then
            git push fcl
        else
            git push --all --force fcl
        fi
    )
}

alias civfcl="git commit -av && gitpushfcl"
alias amendfcl="git commit --amend -C HEAD -a && gitpushfcl"
fixupfcl () {
    git commit -a --fixup "${1?Need ref}" && \
        gitpushfcl
}

ssh () {
    if [[ $TERM == screen* || $TERM == tmux* ]]; then
        TERM=screen command ssh "$@"
    elif [[ $TERM == xterm-kitty ]]; then
        TERM=xterm-256color command ssh "$@"
    else
        command ssh "$@"
    fi
}

gsissh () {
    local gsissh ret
    gsissh=`/usr/bin/which gsissh`; ret=$?
    if [[ $ret != 0 ]]; then
        gsissh=osgrun\ gsissh
    fi
    if [[ $TERM == screen* || $TERM == tmux* ]]; then
        TERM=screen $gsissh "$@"
    elif [[ $TERM == xterm-kitty ]]; then
        TERM=xterm-256color $gsissh "$@"
    else
        $gsissh "$@"
    fi
}

getrpmmacro () {
    rpm --showrc | less "+/^-\d+: $1"
}

if [ X${ZSH_NAME-} != X ]; then
    zstyle ':completion:*' max-errors 0 numeric
    ssource "${ZDOTDIR:-${DOTDIR:-$HOME}}/.shell/local/osg_build_wrapper"
fi

cdpkg () {
    local pkg=${1?Need package}
    pkg=${pkg%/}
    local pkgdir
    local pkgtmpdir
    local ret=0

    if [[ -d ~/nn/gitpkgs ]]; then
        pkgdir=~/nn/gitpkgs/$1
    elif [[ -d ~/vdt/gitpkgs ]]; then
        pkgdir=~/vdt/gitpkgs/$1
    fi
    pkgtmpdir=~/shm/.cdpkg.$1

    if [[ -d $pkgdir ]]; then
        cl "$pkgdir"; ret=$?
    else
        if ask_yn "Dir doesn't exist; clone it?"; then
            rm -rf "$pkgtmpdir"
            mkdir -p "$pkgtmpdir" || return 1
            cd "$pkgtmpdir" || return 1
            clonepkg "$pkg" || return 1
            rsync -r "$pkg/" "$pkgdir" || return 1
            cd ..
            rm -rf "$pkgtmpdir"
            cl "$pkgdir"; ret=$?
        fi
    fi
    if [[ $ret == 0 ]]; then
        local len file
        for file in README.md README.txt README; do
            if [[ -f $file ]]; then
                len=$(wc -l "$file" | cut -d' ' -f1)
                head -n10 "$file"
                if [[ $len -gt 10 ]]; then
                    echo "..."
                fi
            fi
        done
    else
        return $ret
    fi
}

cdpkg2 () {
    local pkg=${1?Need package}
    local dirs

    pushd ~/nn/redhat
    dirs=(branches/*/$pkg)
    popd

    select d in ${dirs[*]}; do
        cl ~/nn/redhat/"$d"
        break
    done
}

cdsw () {
    if (( $# > 0 )); then
        cl ~/work/software/${1#osg-}*/
    else
        select d in ~/work/software/*/; do
            cl "$d"
            break
        done
    fi
}

cddkr () {
    if (( $# > 0 )); then
        cl ~/work/software/docker-${1#docker-}*/
    else
        select d in ~/work/software/docker-*/; do
            cl "$d"
            break
        done
    fi
}

cddoc () {
    if (( $# > 0 )); then
        cl ~/vdt/docs/$1*
    else
        select d in ~/vdt/docs/*; do
            cl "$d"
            break
        done
    fi
}

cdvm () {
    cd ~/vagrant/VMS/${1?Need VM}
}


# load completion for condor
if typeset -f complete && wehave condor_q; then
    ssource /etc/bash_completion.d/condor
fi


##################################
#        osg_build_wrapper
##################################

osgbuildquilt__init_repo () {
    set -e
        git init .
        printf "%s\n" '' .pc patches series *.patch >> .git/info/exclude
        git add .
        git commit -aqm "Base sources"
        git tag base
    set +e
}


osgbuildquilt__fix_line_endings () {
    local -a filelist
    local oldifs

    [[ -e patches ]] || return

    oldifs=$IFS
    IFS=$'\n'

    quilt push -aq > /dev/null
    filelist=(`quilt files -a`)
    quilt pop -aq > /dev/null
    xargs dos2unix <<<"${filelist[@]}"
    pushd patches
        quilt series | xargs dos2unix
    popd

    IFS=$oldifs
}


osgbuildquilt__apply_patch () {
    local patchname patchnum
    patchnum="$1"
    patchname=`quilt top`

    if grep -qE '^From [0-9a-f]{40} ' <(quilt header | head -1); then
        # git made this patch
        # Undo quilt push's changes to the working tree
        # and use git-am to apply the patches instead
        git stash -q
        git clean -df
        if git am -q "patches/$patchname"; then
            git stash drop -q || :
        else
            # fallback to previous method if git am fails
            git am --abort || :
            git reset --hard HEAD || :
            git stash pop -q || :
            git add . || :
            git commit -qm "${patchname%.patch}"
        fi
    else
        git add . || :
        git commit -qm "${patchname%.patch}"
    fi
    git tag "patch$patchnum"
}


osgbuildquilt () {
    local lineendings=false
    local option OPTARG OPTIND
    while getopts ":l" option; do
        case $option in
            l) lineendings=true ;;
            \?) echo "$OPTARG not implemented. Ignoring.";;
        esac
    done

    shift $(($OPTIND - 1))

    local dir
    dir=$(readlink -f "${1:-$(_getpkgdir)}")

    cd "$dir" # osg-build will nuke the _quilt dir so get out of it first
    osg-build quilt "$dir" || return
    cd "$dir/_quilt" || return

    local subdir
    subdir=$(ls -dt ./*/ | grep -Ev 'BUILDROOT|RPM|SPEC|SOURCE' | head -1)

    [[ -d $subdir ]] || return
    cd "$subdir" || return

    # Use the quilt-git script if available
    if wehave quilt-git; then
        local -a args
        args=(-p -f)
        if $lineendings; then
            args+=(-l)
        fi
        quilt-git "${args[@]}" || return
    else
        # Fall back to our own implementation if quilt-git is missing

        osgbuildquilt__init_repo || return

        if $lineendings; then
            osgbuildquilt__fix_line_endings
        fi

        local patchnum=1
        while quilt push; do
            osgbuildquilt__apply_patch "$patchnum"
            (( patchnum++ ))
        done

        quilt next &> /dev/null || git tag fullypatched -m 'Fully patched'

        if [[ -L patches ]]; then
            # Turn 'patches' dir from a symlink (which creates a loop) to a regular
            # directory
            local patches_tempdir=$(mktemp -d)
            quilt series | while read line; do
                cp -f "patches/$line" "$patches_tempdir/$line" || :
            done
            rm -f patches
            mv "$patches_tempdir" patches
        fi
    fi
    echo

    if wehave git-setemail; then
        git-setemail work
    fi

    ls
}

ob () {
    local cmd=""
    local -a args
    local option OPTARG OPTIND
    local do_ding=false
    local getlogs=false
    local vcsbuild=true
    local lineendingsarg=
    local ret
    while getopts ":789gklpqr:sv" option; do
        case $option in
            k) cmd="koji";;
            p) cmd="prepare";;
            q) cmd="quilt";;
            s) args+=(--scratch); vcsbuild=false;;
            g) args+=(--scratch --getfiles); getlogs=true; vcsbuild=false;;
            r) case $OPTARG in
                    35|3.5)             args+=(--repo=osg-3.5);;
                    36|3.6)             args+=(--repo=osg-3.6);;
                    23m*)               args+=(--repo=23-main);;
                    dev*)               args+=(--repo=devops);;
                    35u*|3.5-u*)        args+=(--repo=3.5-upcoming);;
                    36u*|3.6-u*)        args+=(--repo=3.6-upcoming);;
                    23u*)               args+=(--repo=23-upcoming);;
                    23i*)               args+=(--repo=23-internal);;
                    *)                  args+=(--repo="$OPTARG");;
                esac;;
            7) args+=(--el7);;
            8) args+=(--el8);;
            9) args+=(--el9);;
            v) args+=(--vcs); vcsbuild=true;;
            l) lineendingsarg=-l;;
            \?) echo "$OPTARG not implemented. Ignoring."
                # ^ zsh getopts handles missing args by putting '?' in $Option
                # and the letter the user specified into $OPTARG
                ;;
        esac
    done

    if [[ -z $cmd ]]; then
        echo "No command specified"
        return 2
    fi

    shift $(($OPTIND - 1))

    local dir newdir
    if [[ -n ${1-} ]]; then
        dir=$(readlink -f "$1")
        shift
    else
        dir=$(_getpkgdir)
    fi
    if (( ${#} > 0 )); then
        echo "Too many parameters - this function can only do one package at a time"
        return 2
    fi

    if [[ $cmd = quilt ]]; then
        osgbuildquilt ${lineendingsarg+-l} "$dir"
    else
        [[ $cmd = koji ]]             && \
            $vcsbuild                 && \
            type svn-url &> /dev/null && \
            newdir="$(svn-url "$dir")"

        [[ -n $newdir ]] && dir="$newdir"

        osg-build $cmd "${args[@]}" "$dir"; ret=$?
        if (( ret == 0 )); then
            if $do_ding && type ding &> /dev/null; then
                ding "osg-build for $dir is done"
            fi
        else
            if $getlogs && ! $vcsbuild; then
                less "$dir"/_build_results/*/*.log
            fi
        fi
    fi
}

alias xob='xhold ob'

alias obq=osgbuildquilt




# vim:ft=zsh
