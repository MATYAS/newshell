if wehave kubectl; then
    if [[ -n $KUBECONFIG ]]; then
        _precmd_kube () {
            local kube_context kube_namespace
            kube_context=$(kubectl config current-context 2>/dev/null)
            if [[ -n $kube_context ]]; then
                kube_namespace=$(kubectl config get-contexts "$kube_context" --no-headers | awk '{print $5}')
                if [[ -n $kube_namespace ]]; then
                    pmt_kubeinfo="[${kube_context}(${kube_namespace})] "
                else
                    pmt_kubeinfo="[${kube_context}] "
                fi
            else
                pmt_kubeinfo=""
            fi
        }
    fi
    sk8 () {
        local namespace
        namespace=$1
        shift &>/dev/null || :
        if [[ $namespace == "-n" ]]; then
            namespace=$1
            shift &>/dev/null || :
            echo >&2 "Using namespace $namespace"
        elif [[ $namespace == "-" ]]; then
            namespace=
            shift &>/dev/null || :
            echo >&2 "Not using a namespace"
        else
            echo >&2 "Using namespace $namespace"
        fi
        sudo kubectl --kubeconfig /etc/kubernetes/admin.conf "${namespace+-n "$namespace"}" "$@"
    }
fi

