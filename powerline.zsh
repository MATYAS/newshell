function powerline_precmd() {
    local err=$?
    powermod_user=user,
    if [[ $USER == m[es] || $USER == [Mm]atyas* ]]; then
        powermod_user=
    fi
    local jobs=${${(%):-%j}:-0}
    PS1="$(powerline-go -error $err -shell zsh -jobs $jobs -cwd-max-depth 8 -newline -max-width 90 -hostname-only-if-ssh -modules time,venv,${powermod_user}host,ssh,cwd,perms,git,hg,jobs,exit,root 2>&1)"
}

function install_powerline_precmd() {
  for s in "${precmd_functions[@]}"; do
    if [ "$s" = "powerline_precmd" ]; then
      return
    fi
  done
  precmd_functions+=(powerline_precmd)
}

if [ "$TERM" != "linux" ]; then
    install_powerline_precmd
fi

