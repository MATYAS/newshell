winstartup () {
    sshagent
    addkeys
    do_winstartup
}


if wehave fzf; then
    cdsw () {
        local newdir
        local cmd=(find -L ~/workrepos/ ~/repos2/ -maxdepth 1 -name '.*' -prune -o -name '_*' -prune -o -type d -print)
        if (( $# > 0 )); then
            newdir=$("$cmd[@]" | fzf -1 -q "$*")
        else
            newdir=$("$cmd[@]" | fzf)
        fi
        if [[ -d $newdir ]]; then
            cd "$newdir" && ls
        fi
    }
else
    cdsw () {
        local oldnullglob="nonullglob"
        setopt | grep -q '^nullglob' && oldnullglob="nullglob"
        setopt nullglob
        if (( $# > 0 )); then
            cl ~/work/software/${1#osg-}*/ ~/repos2/${1#osg-}*/
            setopt $oldnullglob
        else
            select d in ~/work/software/*/ ~/repos2/*/; do
                cl "$d"
                break
            done
            setopt $oldnullglob
        fi
    }
fi


cddkr () {
    local oldnullglob="nonullglob"
    setopt | grep -q '^nullglob' && oldnullglob="nullglob"
    setopt nullglob
    if (( $# > 0 )); then
        cl ~/work/software/docker-${1#docker-}*/ ~/repos2/docker-${1#docker-}*/
    else
        select d in ~/work/software/docker-*/ ~/repos2/docker-*/; do
            cl "$d"
            break
        done
    fi
}


cddoc () {
    local oldnullglob="nonullglob"
    setopt | grep -q '^nullglob' && oldnullglob="nullglob"
    setopt nullglob
    if (( $# > 0 )); then
        cl ~/repos/docs/${1}*/
        setopt $oldnullglob
    else
        select d in ~/repos/docs/*/; do
            cl "$d"
            break
        done
        setopt $oldnullglob
    fi
}


if wehave kubectl && [[ -n ${KUBECONFIG:-$HOME/.kube/config} ]]; then
    _precmd_kube () {
        local kube_context kube_namespace
        kube_context=$(kubectl config current-context 2>/dev/null)
        if [[ -n $kube_context ]]; then
            kube_namespace=$(kubectl config get-contexts "$kube_context" --no-headers | awk '{print $5}')
            if [[ -n $kube_namespace ]]; then
                pmt_kubeinfo="[🗃 ${kube_context}(${kube_namespace})] "
            else
                pmt_kubeinfo="[🗃 ${kube_context}] "
            fi
        else
            pmt_kubeinfo=""
        fi
    }
fi


