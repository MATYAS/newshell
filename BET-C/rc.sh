_os=`uname -o`
if test XMsys = "X$_os"; then
    # Git Bash here
    :
elif test XGNU/Linux = "X$_os"; then
    # Bash on Ubuntu on Windows here
    :
fi

case ${WSL_DISTRO_NAME-} in
    OracleLinux_8_6|AlmaLinux-8)
        WinMount=X
        ;;
    Ubuntu-22.04)
        WinMount=Y
        ;;
    Ubuntu|AlmaLinux9)
        WinMount=Z
        ;;
esac

winpath () {
    local p
    p=${1-$PWD}
    p=$(readlink -f "$p" | tr '/' '\\')
    if test "X\\mnt\\c" == "X$p"; then
        p=C:\\
    elif test "X${p#\\mnt\\c\\}" != "X$p"; then
        p=C:${p#\\mnt\\c}
    else
        p=${WinMount}:$p
    fi
    printf "%s\n" "$p"
}

winvp () {
    winvim "$(winpath "$*")"
}
