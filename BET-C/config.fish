function update_swbasecached_if_needed
    if pushd ~/repos2/mydocker/swbasecached
        ./build-or-update
        popd
    end
end

function winstartup
    sshagent
    ssh-add ~/.ssh/id_{rsa_new,ecdsa,ecdsa_n2_2022}
    do_winstartup
end

if hash podman &>/dev/null
    abbr pod "podman"
    abbr spod "sudo podman"
    abbr podimg "podman image"
    abbr podexec "podman exec"
    abbr podrm "podman rm"
    abbr podrmi "podman rmi"
    abbr podps "podman ps"
    abbr podpsa "podman ps -a"
    abbr podrun "podman run"
    abbr podrunit "podman run -i -t"
    abbr podruntmp "podman run -i -t --rm"

    function podrunn
        podman run -i -t --hostname $argv[1] --name $argv[1] $argv[2..(count $argv)]
    end

    function podruntmpn
        podman run --rm -i -t --hostname $argv[1] --name $argv[1] $argv[2..(count $argv)]
    end
end

if hash exec-osg-build &>/dev/null
    abbr ok "exec-osg-koji"
    abbr osg-build "exec-osg-build"
end

