_os=`uname -o`
is_wsl2 () {
    expr match "$(uname -r)" ".*WSL2" >/dev/null
}

if test XMsys = "X$_os"; then
    # Git Bash here
    :
elif test X != "X${WSL_DISTRO_NAME-}"; then
    # WSL here
    export DISPLAY="${DISPLAY-:0.0}"
    is_wsl2 || export LIBGL_ALWAYS_INDIRECT=1
    xrdb -merge ~/.Xresources >/dev/null 2>&1 || :

    [ -d /dev/shm/$(id -un) ] || install -d -m 0700 /dev/shm/$(id -un)
    if is_wsl2; then
        if command -v joe >/dev/null 2>&1; then
            export GIT_EDITOR=joe
        fi
    fi

    pathprepend "${DOTDIR:-$HOME}/repos/newutils/public"
    pathprepend "${DOTDIR:-$HOME}/repos/newutils/public/tiny"
    pathprepend "${DOTDIR:-$HOME}/repos/newutils/csl"
fi

export EMAIL=matyas@cs.wisc.edu
