# Usage: ssource filename
ssource () {
    if [ -r "${1-}" ]; then
        local _oldopts=$-
        set +u # unset nounset
        set +C # unset noclobber
        . "${1-}"
        expr $_oldopts : '.*u' > /dev/null && set -u
        expr $_oldopts : '.*C' > /dev/null && set -C
    fi
}

DOTDIR=${DOTDIR:-$HOME}
SHELLDIR=$DOTDIR/.shell

# Because OS X does not do `hostname -d`
hostname_d () {
    local hostname
    hostname=$(uname -n)
    echo "${hostname#*.}"
}

# Detect which terminal we're running (if any); if we don't have a terminfo for that term, use a compatible one that we do have terminfo for
if [ "X${TERM:-}" != X ]; then                                  # TERM is defined and not empty
    export TERM
    # ubuntu has xterm+256color not xterm-256color
    _TERM_plus=$(echo "$TERM" | tr "-" "+")
    until (/bin/ls /usr/share/terminfo/*/"$TERM" || /bin/ls /usr/share/terminfo/*/"$_TERM_plus") >/dev/null 2>&1; do
        if [ "${TERM}" != "${TERM%-it}" ]; then                 # TERM ends with -it
            TERM=${TERM%-it}
        elif [ xterm-kitty = "${TERM}" ]; then                  # this one often doesn't exist
            TERM=xterm-256color
        elif [ alacritty = "${TERM}" ]; then                    # or this
            TERM=xterm-256color
        elif [ tmux-256color = "${TERM}" ]; then
            TERM=screen-256color
        elif [ "${TERM}" != "${TERM%-256color}" ]; then         # maybe we don't have a 256color version?
            TERM=${TERM%-256color}
        elif [ "${TERM}" = vt100 ]; then                        # we don't even have that??
            TERM=dumb
            break                                               # I'm done
        else                                                    # second-to-last-resort
            TERM=vt100
        fi
    done
fi


# if in a terminal
if [ -t 0 ]; then
    stty -ixon -ixoff
    stty stop undef

    if [ "X${GPG_TTY-}" = X ]; then
        GPG_TTY=$(tty)
        export GPG_TTY
    fi
fi

export COLUMNS
export LINES


# Detect which shell we're running
# If this isn't accurate enough, I can do tricky things with /proc/$$ ...
if [ "X${BASH-}" != X ]; then
    SHELLTYPE=bash
    ### Advanced (i.e. not sh-compatible) functions ###
    . "$SHELLDIR"/advrc.sh
elif [ "X${ZSH_NAME-}" != X ]; then
    SHELLTYPE=zsh
    ### Advanced (i.e. not sh-compatible) functions ###
    . "$SHELLDIR"/advrc.sh
else
    SHELLTYPE=sh
fi




###############################################################################
# SHELL ALIASES AND FUNCTIONS
###############################################################################

#==============================================================================
# safe_aliases_and_functions/
#==============================================================================

#------------------------------------------------------------------------------
# aliases
#------------------------------------------------------------------------------

# default args
alias mv="mv -i"

wehave gdb && alias gdbt="gdb -q -tui"
if wehave grep; then
    alias grep="grep --color=auto"
    alias egrep="egrep --color=auto"
    alias fgrep="fgrep --color=auto"
    alias igrep="grep -i"
    alias eigrep="egrep -i"
    alias iegrep="egrep -i"
fi
wehave less && alias more="less -XE -+S"

# ls
# $ls_options isn't used by ls, it's just a convenient way of storing the options. Thanks Carl for the idea!
if test dumb != "$TERM"; then
    if test X"${OSTYPE#freebsd}" != X"${OSTYPE}" ||
        test X"${OSTYPE#darwin}" != X"${OSTYPE}"; then
        ls_options='-pG'
        ll_options='-lG'
    else
        ls_options='-pv --color=auto --quoting-style=shell --group-directories-first'
        ll_options='-lv --color=auto --quoting-style=shell --group-directories-first'
    fi
else
    ls_options=""
    ll_options="-l"
fi
alias ls="/bin/ls \$ls_options"
alias lsdirs='ls -d $(find . -maxdepth 1 -type d -printf "%f ")'
alias ll="/bin/ls \$ll_options"
if wehave eza; then
    alias ezal="eza -lg --git"
    alias exal=ezal
elif wehave exa; then
    alias exal="exa -lg --git"
fi
if ! wehave bat; then
    if wehave vim; then
        alias bat="vim -R"
    elif wehave vi; then
        alias bat="vi -R -cset\ nocompatible"
    elif wehave less; then
        alias bat=less
    fi
else
    test -f ~/.light && read -r LIGHT < ~/.light
    LIGHT=${LIGHT#LIGHT=}
    if test "Xtrue" = "X$LIGHT"; then
        export BAT_THEME=GitHub
    fi
fi
alias l.="ls -d .*"
alias la="ls -A"

# perl
if wehave perl; then
    alias pdeparse='perl -MO=Deparse'
    alias pdoc="perldoc"
    alias pdocf="perldoc -f"
    alias perlde0="perl -de0" # perl 'interpreter'
    alias ptkdb="perl -d:ptkdb" # perl tk-based debugger
    pdocs() { perldoc "perl$1"; }
fi

# other util shorthand
wehave diff && \
    alias makepatch='LC_ALL=C TZ=UTC0 diff -NarU5 --exclude=.svn --exclude=CVS --exclude=.git --exclude=_darcs --exclude=.hg'
wehave python2 && alias py2=python2
wehave python3 && alias py3=python3
wehave python3.6 && alias py36=python3.6
wehave python3.9 && alias py39=python3.9
wehave python3.10 && alias py310=python3.10
wehave ipython && alias ipy=ipython
wehave ipython2 && alias ipy2=ipython2
wehave ipython3 && alias ipy3=ipython3
if wehave svn; then
    alias svndm="svn merge --dry-run"
    alias svnmeld="svn diff --diff-cmd meld -x ''"
fi
alias setx="chmod +x"
if wehave vim; then
    alias vimr="vim -R"
    alias vt="vim -T vt100"
    alias vtdiff="vimdiff -T vt100"
    wehave gvim && alias gvimr="gvim -R"
fi
wehave ed && alias ed="ed -p:"
if wehave git; then
    alias g="git"
    alias gwork="EMAIL=matyas@cs.wisc.edu git"
    alias gpers="EMAIL=matyasbot@gmail.com git"
    alias gsvn="git svn"
    alias ghe="git help"
    alias gann="git annex"
    alias ggr="git grep"
    alias ggrv="ggr -Ovi"
    cdgit () { cd "$(git rev-parse --show-toplevel)${1+/$1}"; }
fi
wehave ssh && alias sshr="ssh -l root"
wehave qvim && alias qv=qvim
unalias vi >/dev/null 2>&1 || : # /etc/profile.d might alias this
if wehave tmux; then
    alias tmuxinner="tmux -f $DOTDIR/.tmuxinner.conf"
    alias tmuxatnew="tmux attach || tmux new"
fi
alias mkts=make_testscript

alias sort="sort -b"
# ^ fixes sort's horrible behavior when sorting text with nicely lined up fields

alias s='sudo '

# this makes file names not suck when downloading from, e.g. goc tickets
alias wgetcd="wget --content-disposition"

# cd
alias back="popd"
alias prevd="popd"  # fish
alias up="cd .."
alias dirh=dirs  # dir history/dir stack

alias kcsl='kinit -l 2d -f matyas@CS.WISC.EDU'
unset -f reloadshell >&/dev/null || :
unalias reloadshell >&/dev/null || :
reloadshell () {
    local l=
    $is_login && l=-l
    if test 0 -eq "$(jobs | wc -l)"; then
        exec $SHELL -i $l
    else
        echo >&2 "You still have jobs running!"
        return 1
    fi
}
alias cdphys='cd "$(pwd -P)"'

ask_yn () {
    echo "$@" "?"
    while read -r; do
        case $REPLY in
            [Yy]*) return 0;;
            [Nn]*) return 1;;
            *) echo "Enter yes or no";;
        esac
    done
    return 2  # EOF
}

if wehave ack; then
    alias ackl='ack --pager="less -R"'
    alias acki='ack -i'
fi
if wehave ag; then
    alias agl='ag --pager="less -R"'
    alias agi='ag -i'
fi

# other shell
alias basepwd='echo ${PWD##*/}'

# lolbash
alias donotwant='/bin/rm -rf'
canihaz () { mkdir -p "$*" && cd "$*" ; }

# # rcs
# # Set default options when creating a new rcs file (-ko = no keyword expansion,
# # -U = no strict locking).
# alias rcsi='rcs -i -ko -U'
# # Disable implicit rcs file creation, requiring me to use rcsi or rcsnew for
# # the initial commit.
# alias ci='ci -j'
# # Disable keyword expansion.
# alias co='co -ko'


# a neat trick to allow me to use aliases in run and others
# (does not work with sudo)
alias run='run '
alias tnew='tnew '
alias tspl='tspl '
alias tvspl='tvspl '

unalias which >/dev/null 2>&1

dash () { ( unset PS1 PS2 PS4 PROMPT_COMMAND; command dash "$@" ); }



if wehave dnf; then
    # Source: https://github.com/robbyrussell/oh-my-zsh/blob/master/plugins/dnf/dnf.plugin.zsh
    # except I put sudo before each of them so they all use the same cache
    alias sdnf="sudo dnf"
    alias dnfl="sudo dnf list"                  # List packages
    alias dnfli="sudo dnf list installed"       # List installed packages
    alias dnfgl="sudo dnf grouplist"            # List package groups
    alias dnfp="sudo dnf info"                  # Show package information
    alias dnfs="sudo dnf search"                # Search package
    alias dnfu="sudo dnf upgrade"               # Upgrade package
    alias dnfds="sudo dnf distro-sync"          # Upgrade (mostly)
    alias dnfunk="sudo dnf distro-sync --exclude=kernel\* --exclude=\*firmware\* --exclude=dbus\*"  # Upgrade (mostly), skip stuff that might need reboot
    alias dnfi="sudo dnf install"               # Install package
    alias dnfgi="sudo dnf groupinstall"         # Install package group
    alias dnfr="sudo dnf remove"                # Remove package
    alias dnfgr="sudo dnf groupremove"          # Remove package group
    alias dnfc="sudo dnf clean all"             # Clean cache
    alias dnfh="sudo dnf history"               # History operations
    alias dnfe="sudo dnf erase"                 # Remove package
    alias dnfco="sudo dnf copr"                 # COPR operations
    alias dnfmarki="sudo dnf mark install"      # Mark packages as user-installed
    alias dnfmarkr="sudo dnf mark remove"       # Mark packages as not user-installed
    alias dnfar="sudo dnf autoremove"           # Remove unneeded leaf packages
fi

if wehave yum; then
    alias syum="sudo yum"
    alias yuml="sudo yum list"                  # List packages
    alias yumli="sudo yum list installed"       # List installed packages
    alias yumgl="sudo yum grouplist"            # List package groups
    alias yump="sudo yum info"                  # Show package information
    alias yums="sudo yum search"                # Search package
    alias yumu="sudo yum upgrade"               # Upgrade package
    alias yumunk="sudo yum distro-sync --exclude=kernel\* --exclude=\*firmware\* --exclude=dbus\*"  # Upgrade (mostly), skip stuff that might need reboot
    alias yumi="sudo yum install"               # Install package
    alias yumgi="sudo yum groupinstall"         # Install package group
    alias yumr="sudo yum remove"                # Remove package
    alias yumgr="sudo yum groupremove"          # Remove package group
    alias yumc="sudo yum clean all"             # Clean cache
    alias yumh="sudo yum history"               # History operations
    alias yume="sudo yum erase"                 # Remove package
fi

if wehave systemctl; then
    sysstart () {
        systemctl start "$@" && \
            sleep 1 && \
            systemctl status "$@"
    }

    sysstop () {
        systemctl stop "$@" && \
            sleep 1 && \
            systemctl status "$@"
    }

    sysstatus () {
        systemctl status "$@"
    }
elif wehave service; then
    sysstart () {
        service "$1" start && \
            sleep 1 && \
            service "$1" status
    }

    sysstop () {
        service "$1" stop && \
            sleep 1 && \
            service "$1" status
    }

    sysstatus () {
        service "$1" status
    }
fi


virtenv () {
    local activatepath
    if [ -r ./activate ]; then
        activatepath=./activate
    else
        local oldwd found=false
        oldwd=$PWD

        until [ . -ef / ]; do
            if [ -r bin/activate ]; then
                activatepath=$(pwd -P)/bin/activate
                found=true
                break
            elif [ -r venv-linux/bin/activate ]; then
                activatepath=$(pwd -P)/venv-linux/bin/activate
                found=true
                break
            elif [ -r venv/bin/activate ]; then
                activatepath=$(pwd -P)/venv/bin/activate
                found=true
                break
            elif [ -r env/bin/activate ]; then
                activatepath=$(pwd -P)/env/bin/activate
                found=true
                break
            elif [ -r .venv/bin/activate ]; then
                activatepath=$(pwd -P)/.venv/bin/activate
                found=true
                break
            fi
            cd ..
        done
        cd "$oldwd" || :
        if ! $found; then
            echo >&2 "not found"
            return 1
        fi
    fi
    echo "using $activatepath"
    ssource "$activatepath"
}

wehave tmuxie &&
    virtmux () {
        local socketname
        virtenv && \
            socketname=${PWD##*/} && \
            socketname=${socketname#.} && \
            tmuxie "$socketname"
    }


alias pathapwd='pathappend "`pwd`" PATH'

setdircolors () {
    eval $(dircolors --sh ${DOTDIR:-$HOME}/.shell/DIR_COLORS.${1#DIR_COLORS.})
}


mergeallmasters () {
    git branch -r | grep master | grep -v HEAD | xargs git merge
}


if wehave ansible; then
    alias ansi=ansible
    alias ansik="ansible -K"
    alias ansikb="ansible -Kb"
    alias ansip=ansible-playbook
    alias ansipdry="ansible-playbook -CD"
    alias ansipk="ansible-playbook -K"
    alias ansipkdry="ansible-playbook -K -CD"
fi


if wehave docker; then
    alias doc=docker
    alias docimg="docker image"
    alias docexec="docker exec"
    alias docrm="docker rm"
    alias docrmi="docker rmi"
    alias docbuild="docker build"
    alias docps="docker ps"
    alias docpsa="docker ps -a"
    alias docrun="docker run"
    alias docrunit="docker run -i -t"
    alias docruntmp="docker run --rm -i -t"
    alias docrunp="docker run --pull=always"
    alias docrunitp="docker run --pull=always -i -t"
    alias docruntmpp="docker run --pull=always --rm -i -t"
    alias docimgshort="docker image ls --format 'table {{.Repository}}:{{.Tag}}'"
fi

if wehave podman; then
    alias pod=podman
    alias spod="sudo podman"
    alias podimg="podman image"
    alias podexec="podman exec"
    alias podrm="podman rm"
    alias podrmi="podman rmi"
    alias podbuild="podman build"
    alias podps="podman ps"
    alias podpsa="podman ps -a"
    alias podrun="podman run"
    alias podrunit="podman run -i -t"
    alias podruntmp="podman run --rm -i -t"
    alias podrunp="podman run --pull=always"
    alias podrunitp="podman run --pull=always -i -t"
    alias podruntmpp="podman run --pull=always --rm -i -t"
    alias podimgshort="podman image ls --format 'table {{.Repository}}:{{.Tag}}'"

    podruntmpn () {
        local name="$1"; shift
        podman run --rm -i -t --hostname "$name" --name "$name" "$@"
    }

    podrunn () {
        local name="$1"; shift
        podman run -i -t --hostname "$name" --name "$name" "$@"
    }
fi


if wehave kubectl; then
    ssource "$SHELLDIR"/kubealiases.sh
fi


if wehave plocate; then
    alias locate=plocate
fi

###############################################################################
# END
###############################################################################

# hostname -f is slow when the network is shitty
full_hostname=$(uname -n)
short_hostname=${full_hostname%%.*}
domain_name=${full_hostname#*.}


[ -r "$SHELLDIR/$domain_name/rc.sh" ]    && . "$SHELLDIR/$domain_name/rc.sh"
[ -r "$SHELLDIR/$short_hostname/rc.sh" ] && . "$SHELLDIR/$short_hostname/rc.sh"
if [ "$full_hostname" != "$short_hostname" ]; then
    [ -r "$SHELLDIR/$full_hostname/rc.sh" ]  && . "$SHELLDIR/$full_hostname/rc.sh"
fi


## TODO: This makes mc hang on startup
# if wehave fish && [ "X${nofish}" = X ] && [ ! -e ~/.nofish ]; then
#     export oldshell=$SHELL
#     if expr match X$- ".*l.*" >/dev/null; then
#         export login=1
#         exec fish -il
#     else
#         exec fish -i
#     fi
# fi


# vim:ft=sh
