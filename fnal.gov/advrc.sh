pathprepend "$m/bin" PATH

mts=$m/testscripts
mtss=$m/testscripts/setup.sh

if test Xfcluigpvm = X"`expr substr \"\`hostname\`\" 1 9`"; then
    test "X$ONE_XMLRPC" != X || ssource /etc/profile.d/one4x.sh > /dev/null
    test "X$ONE_AUTH" != X || ssource /etc/profile.d/one4x_user_credentials.sh > /dev/null
fi

vim () {
    if type vim &> /dev/null; then
        if test -e "$m/.vimrc"; then
            command env HOME=~matyas vim -u "$m/.vimrc" "$@"
        else
            command vim "$@"
        fi
    fi
}

# bashisms are fine in these functions because rpm systems have bash
_disablerepo_do_disable () {
    local repoName="$1"
    local realRepoFile="$2"

    echo Disabling $repoName in $realRepoFile
    sed -i '/\['$repoName'\]/,/\[.*\]/s/enabled[ ]*=[ ]*1/enabled=0/' "$realRepoFile"
}

_enablerepo_do_enable () {
    local repoName="$1"
    local realRepoFile="$2"

    echo Enabling $repoName in $realRepoFile
    sed -i '/\['$repoName'\]/,/\[.*\]/s/enabled[ ]*=[ ]*0/enabled=1/' "$realRepoFile"
}

_get_realRepoFile () {
    # Find reponame in a repofile
    local repoName="${1?Need repoName}"
    local repoFile="${2-}"

    if [[ -n $repoFile ]]; then
        if [[ -e $repoFile ]]; then
            if fgrep -q "[$repoName]" "$repoFile"; then
                echo $repoFile
                return 0
            else
                return 1
            fi
        fi

        repoFile=/etc/yum.repos.d/${repoFile#/etc/yum.repos.d}
        repoFile=${repoFile%.repo}.repo
        if [[ -e $repoFile ]]; then
            if fgrep -q "[$repoName]" "$repoFile"; then
                echo $repoFile
                return 0
            else
                return 1
            fi
        fi
    fi
    for aRepoFile in /etc/yum.repos.d/*.repo; do
        if fgrep -q "[$repoName]" "$aRepoFile"; then
            echo $aRepoFile
            return 0
        fi
    done
    return 1
}

disablerepo () {
    # Disable a repo in a repo config file
    local repoName="${1?Need reponame}"
    local repoFile="${2-}"

    local realRepoFile="$(_get_realRepoFile "$repoName" "$repoFile")"
    if [[ -z $realRepoFile ]]; then
        echo Could not find $repoName
        return 1
    fi

    _disablerepo_do_disable "$repoName" "$realRepoFile"
}

enablerepo () {
    # Enable a repo in a repo config file
    local repoName="${1?Need reponame}"
    local repoFile="${2-}"

    local realRepoFile="$(_get_realRepoFile "$repoName" "$repoFile")"
    if [[ -z $realRepoFile ]]; then
        echo Could not find $repoName
        return 1
    fi

    _enablerepo_do_enable "$repoName" "$realRepoFile"
}

editrepo () {
    # Edit a repo in a repo config file
    local repoName="${1?Need reponame}"
    local repoFile="${2-}"

    local realRepoFile="$(_get_realRepoFile "$repoName" "$repoFile")"
    if [[ -z $realRepoFile ]]; then
        echo Could not find $repoName
        return 1
    fi

    vi "$realRepoFile" '+/\['"$repoName"'\]'
}

enableminefield () {
    enablerepo osg-minefield
}


unalias which &> /dev/null || :
function which () {
    (alias; declare -f) | /usr/bin/which --tty-only --read-alias --read-functions --show-tilde --show-dot "$@"
}
export -f which

alias lftpmv="lftp sftp://matyas@voltimand.cs.wisc.edu"
alias yumclean="yum clean --enablerepo=\* all"
alias runosgtest="(cd /tmp; osg-test -vma --df=test.out)"
alias runoctest="(cd /usr/share/osg-configure/tests; ./run-osg-configure-tests)"

alias repoq="repoquery --plugins"

if [[ $(id -un) == root ]]; then
    alias usematyasproxy="cp /tmp/x509up_u`id -u matyas` /tmp/x509up_u0 && chown root /tmp/x509up_u0"
fi

editcfg () {
    local cfgdir="/etc/${1?Usage: editcfg CFG}/config.d"
    if test -r "$cfgdir"; then
        (cd "$cfgdir" && vim -c ':Explore')
    else
        echo $cfgdir not found
        return 1
    fi
}

viewlogs () {
    local logdir="/var/log/${1:-}"
    vim -R -c ":cd $logdir" -c ":Explore"
}

lesslogs () {
    local logdir="/var/log/${1:-}"
    less "$logdir"/*
}

spray () {
    : ${SPRAY_HOSTS?Need SPRAY_HOSTS}
    local host filepath
    filepath=$(readlink -f "${1?Need file}")
    if [[ ! -f $filepath ]]; then
        echo $1 is not a file
        return 1
    else
        for host in $SPRAY_HOSTS; do
            if [[ $host == $(hostname -s) || $host == $(hostname -f) ]]; then
                continue
            fi
            scp "$1" $host:$filepath
        done
    fi
}

mvlogs () {
    local logdir="/var/log/${1?Need logdir under /var/log}"
    local ownership perms
    if [[ ! -d $logdir ]]; then
        echo "$logdir does not exist"
    else
        ownership=$(stat --format='%u:%g' "$logdir")
        perms=$(stat --format='%a' "$logdir")
        bak -m $logdir
        mkdir $logdir
        chown $ownership $logdir
        chmod $perms $logdir
    fi
}

tmuxvm () {
    local name
    read -r name < /etc/vm_name
    # I can't use new-session -A on tmux 1.6 (SLF6)
    if tmux list-sessions | fgrep "${name}:" &>/dev/null; then
        tmux attach-session -t "$name"
    else
        if [[ $TERM =~ screen* && -e ~/.tmuxinner.conf ]]; then
            tmux -f ~/.tmuxinner.conf set-option -g history-limit 65535 \; new-session -s "$name"
        else
            tmux set-option -g history-limit 65535 \; new-session -s "$name"
        fi
    fi
}

tmuxvmcc () {
    # tmux -CC doesn't work on tmux 1.6 (SLF6) so fall back to regular tmuxvm
    grep 'release 6' /etc/redhat-release && tmuxvm
    local name
    read -r name < /etc/vm_name
    tmux -CC set-option -g history-limit 65535 \; new-session -A -s "$name"
}

sysstart () {
    systemctl start "$@" || echo "ERROR $?"
    sleep 2
    systemctl status "$@"
}

sysrestart () {
    systemctl restart "$@" || echo "ERROR $?"
    sleep 2
    systemctl status "$@"
}

sysstop () {
    systemctl stop "$@" || echo "ERROR $?"
    sleep 2
    systemctl status "$@"
}

sysstatus () {
    systemctl status "$@"
}

# vim:ft=sh
