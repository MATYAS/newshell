pathprepend "${DOTDIR:-$HOME}/repos/newutils/fcl" PATH
pathprepend "${DOTDIR:-$HOME}/repos/newutils/linux" PATH
pathprepend "${DOTDIR:-$HOME}/repos/newutils/public" PATH
pathprepend "${DOTDIR:-$HOME}/repos/newutils/unix" PATH

pathremove "/usr/kerberos/bin" PATH
pathremove "/usr/kerberos/sbin" PATH
