# Usage: ssource filename
ssource () {
    if [[ -r $1 ]]; then
        local _oldopts=$-
        set +u # unset nounset
        set +C # unset noclobber
        . "$1"
        expr $_oldopts : '.*u' > /dev/null && set -u
        expr $_oldopts : '.*C' > /dev/null && set -C
    fi
}

# source bashrc in interactive shell
DOTDIR=${DOTDIR:-$HOME}
[[ $- = *i* ]] && ssource $DOTDIR/.bashrc
ssource $DOTDIR/.profile


# vim:ft=sh
