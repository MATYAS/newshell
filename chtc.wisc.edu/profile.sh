export EMAIL=matyas@cs.wisc.edu
DOTDIR=${DOTDIR:-$HOME}
pathprepend "$DOTDIR/newutils/public" PATH
pathprepend "$DOTDIR/newutils/public/tiny" PATH

if ${is_login:-false}
then
    export factory=gfactory-2.opensciencegrid.org
    export itbfactory=gfactory-itb-1.opensciencegrid.org
    export ospool_cm1=cm-1.ospool.osg-htc.org
    export ospool_cm2=cm-2.ospool.osg-htc.org
    export ospool_itbcm=ospool-itb.osg-htc.org
fi
