cephquota () {
    local p
    p=${1:-.}
    used=$(getfattr -n ceph.dir.rbytes "${p}" | awk -F= '/ceph.dir.rbytes/ {print $2}')
    used=${used//\"/}
    total=$(getfattr -n ceph.quota.max_bytes "${p}" | awk -F= '/ceph.quota.max_bytes/ {print $2}')
    total=${total//\"/}

    printf "Used %d MiB out of %d MiB\n" $(( used >> 20 )) $(( total >> 20 ))
}

if wehave podman; then
    alias aws='podman run -v ~/.kube:/root/.kube -v ~/.aws:/root/.aws --rm -it amazon/aws-cli'
fi
# vim:ft=bash
